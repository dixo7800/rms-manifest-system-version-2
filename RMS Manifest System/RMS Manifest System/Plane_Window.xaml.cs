﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Plane_Window.xaml
    /// </summary>
    public partial class Plane_Window : Window
    {
        public Plane_Window()
        {
            InitializeComponent();
            this.Closed += Plane_Window_Closed;
        }

        private void Plane_Window_Closed(object sender, EventArgs e)
        {
            Home_Window home = new Home_Window();
            home.Show();
            this.Close();
        }

        private void btnAddPlane_Click(object sender, RoutedEventArgs e)
        {
            Plane_RMS plane = new Plane_RMS();
            plane.Plane_ID = txtPlaneID.Text;
            plane.Plane_Make = txtPlaneMake.Text;
            plane.Plane_Model = txtPlaneModel.Text;
            plane.Fuel_Type = txtFuelType.Text;
            plane.Jumper_Capacity = txtJumperCapacity.Text;

            try
            {
                plane.Annual_Date = AnnualDate_Add.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                plane.Annual_Date = dt.Date;

            }

            try
            {
                plane.Service_Date = ServiceDate_Add.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                plane.Service_Date = dt.Date;

            }

            string errorCheckingOutput = plane.ErrorChecking_Add();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                Plane_DB addPlane = new Plane_DB();
                addPlane.Add_Plane(plane);

                txtPlaneID.Clear();
                txtPlaneMake.Clear();
                txtPlaneModel.Clear();
                txtFuelType.Clear();
                txtJumperCapacity.Clear();
                AnnualDate_Add.SelectedDate = new DateTime(1776, 1, 1);
                ServiceDate_Add.SelectedDate = new DateTime(1776, 1, 1);
            }
        }

        private void btnUpdatePlane_Click(object sender, RoutedEventArgs e)
        {
            Plane_RMS plane = new Plane_RMS();
            plane.Plane_ID = PlaneIDLbl_UpdatePlane.Content.ToString();
            plane.Plane_Make = txtPlaneMake_UpdatePlane.Text;
            plane.Plane_Model = txtPlaneModel_UpdatePlane.Text;
            plane.Fuel_Type = txtFuelType_UpdatePlane.Text;
            plane.Jumper_Capacity = txtJumperCapacity_UpdatePlane.Text;

            try
            {
                plane.Annual_Date = AnnualDate_Update.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                plane.Annual_Date = dt.Date;

            }

            try
            {
                plane.Service_Date = ServiceDate_Update.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                plane.Service_Date = dt.Date;

            }

            string errorCheckingOutput = plane.ErrorChecking_Update();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                Plane_DB updatePlane = new Plane_DB();
                updatePlane.Update_Plane(plane);

                PlaneIDLbl_UpdatePlane.Content = "";
                txtPlaneMake_UpdatePlane.Clear();
                txtPlaneModel_UpdatePlane.Clear();
                txtFuelType_UpdatePlane.Clear();
                txtJumperCapacity_UpdatePlane.Clear();
                AnnualDate_Update.SelectedDate = new DateTime(1776, 1, 1);
                ServiceDate_Update.SelectedDate = new DateTime(1776, 1, 1);
            }
        }

        private void Delete_Btn_Click(object sender, RoutedEventArgs e)
        {
            Plane_RMS plane = new Plane_RMS();
            plane.Plane_ID = plane_id_txtbx_delete.Text;

            if (plane.Plane_ID == null || plane.Plane_ID == "")
            {
                MessageBox.Show("A Plane ID must be entered in order for this to function correctly.", "Missing Jumper ID", MessageBoxButton.OK);
            }
            else
            {
                Plane_DB deletePlaneDB = new Plane_DB();
                deletePlaneDB.Delete_Plane(plane);

                plane_id_txtbx_delete.Clear();
            }
        }

        private void btnSearchPlane_Click(object sender, RoutedEventArgs e)
        {
            Plane_RMS plane = new Plane_RMS();
            plane.Plane_ID = plane_ID_txtbx_search.Text;

            if (plane.Plane_ID == null || plane.Plane_ID == "")
            {
                MessageBox.Show("A Plane ID must be entered in order for this to function correctly.", "Missing Jumper ID", MessageBoxButton.OK);
            }
            else
            {
                Plane_DB searchPlaneDB = new Plane_DB();
                DataTable dt = searchPlaneDB.Retrieve_PlaneForDisplay(plane);
                dataGridPlaneSearch.ItemsSource = dt.DefaultView;

                plane_ID_txtbx_search.Clear();
            }
        }

        private void btnAllPlanes_Click(object sender, RoutedEventArgs e)
        {
            /*Plane_DB allPlanes = new Plane_DB();

            Cost_DB allCosts = new Cost_DB();
            DataTable dt = allPlanes.AllPlanes();
            dataGridCostCode.ItemsSource = dt.DefaultView;*/
            MessageBox.Show("Work in Progress", "Coming Soon!", MessageBoxButton.OK);
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

            switch (tabItem)
            {
                case "Update Plane":
                    Update_Window uw = new Update_Window();
                    uw.Table_ID_Lbl.Content = "Plane ID:";

                    try
                    {
                        uw.ShowDialog();
                    }
                    catch(Exception excp)
                    {
                        return;
                    }

                    string Plane_ID_Update = "";
                    Plane_ID_Update = uw.ID_Return;
                    uw.Close();

                    if (Plane_ID_Update == null || Plane_ID_Update == "")
                    {
                        return;
                    }
                    else
                    {
                        Plane_RMS plane = new Plane_RMS();
                        plane.Plane_ID = Plane_ID_Update;
                        Plane_DB ret_DB = new Plane_DB();
                        List<string> retrievedPlaneUpdate = ret_DB.Retrieve_PlaneForUpdate(plane);

                        try
                        {
                            PlaneIDLbl_UpdatePlane.Content = retrievedPlaneUpdate[0];
                            txtPlaneMake_UpdatePlane.Text = retrievedPlaneUpdate[1];
                            txtPlaneModel_UpdatePlane.Text = retrievedPlaneUpdate[2];
                            txtFuelType_UpdatePlane.Text = retrievedPlaneUpdate[3];
                            txtJumperCapacity_UpdatePlane.Text = retrievedPlaneUpdate[4];
                            AnnualDate_Update.Text = retrievedPlaneUpdate[5];
                            ServiceDate_Update.Text = retrievedPlaneUpdate[6];
                        }
                        catch(Exception excp)
                        {
                            MessageBox.Show("The requested data could not be retrieved.", "Error", MessageBoxButton.OK);
                        }

                    }

                    return;

                default:

                    return;
            }
        }
    }
}
