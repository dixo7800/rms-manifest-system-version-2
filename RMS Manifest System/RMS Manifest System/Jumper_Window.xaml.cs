﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Jumper_Window.xaml
    /// </summary>
    public partial class Jumper_Window : Window
    {
        public Jumper_Window()
        {
            InitializeComponent();
            //Loaded += Jumper_Window_Loaded;
            this.Closed += Jumper_Window_Closed;
        }

        private void Jumper_Window_Closed(object sender, EventArgs e)
        {
            Home_Window home = new Home_Window();
            home.Show();
            this.Close();
        }

        private void Jumper_Window_Loaded(object sender, RoutedEventArgs e)
        {
            Jumper_DB db = new Jumper_DB();

            DataTable dt = db.allJumpers();
            if (dt != null)
            {
                //JumperDataGrid.ItemsSource = dt.DefaultView;
            }
        }

        private void btnAddJumper_Click(object sender, RoutedEventArgs e)
        {
            Jumper_RMS jumper = new Jumper_RMS();
            jumper.First_Name = txtFirstName.Text;
            jumper.Last_Name = txtLastName.Text;
            jumper.Street = txtStreet.Text;
            jumper.City = txtCity.Text;
            jumper.State = txtState.Text;
            jumper.Zip = txtZip.Text;
            jumper.Phone_No = txtPhone.Text;
            jumper.Weight = txtWeight.Text;
            jumper.Height = txtHeight.Text;
            jumper.Emergency_Contact = txtEmergencyContact.Text + "@" + emergencyPhoneTxtBx.Text;
            jumper.Apt_No = txtAptNo.Text;
            jumper.UPSA_MEM_NO = txtUSPAMemNo.Text;

            try
            {
                jumper.Date_Of_Birth = DateOfBirth.SelectedDate.Value.Date;
            }
            catch(Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                jumper.Date_Of_Birth = dt.Date;
                
            }

            string errorCheckingOutput = jumper.ErrorChecking_Add();

            if(!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            string foreignKeyCheck = jumper.ForeignKey_Check(txtUSPAMemNo.Text);

            if(!jumper.UPSA_MEM_NO.Equals(null) || !jumper.UPSA_MEM_NO.Equals(""))
            {
                if (!foreignKeyCheck.Equals(""))
                {
                    MessageBox.Show(foreignKeyCheck, "Missing Information", MessageBoxButton.OK);
                    return;
                }
            }
            else
            {
                if (txtAptNo.Text == null || txtAptNo.Text == "")
                {
                    txtAptNo.Text = null;
                    jumper.Apt_No = txtAptNo.Text;
                }

                if (txtUSPAMemNo.Text == "" || txtUSPAMemNo.Text == null)
                {
                    txtUSPAMemNo.Text = null;
                    jumper.Apt_No = txtAptNo.Text;
                }

                //set the appropriate staff attribute for the db - Shawn
                string Staff = "";
                if (staffTrueRadBtn.IsChecked == true)
                {
                    Staff = "TRUE";
                    jumper.Staff = Staff;
                }
                else
                {
                    Staff = "FALSE";
                    jumper.Staff = Staff;
                }

                Jumper_DB add_DB = new Jumper_DB();
                add_DB.AddJumper(jumper);

                txtFirstName.Clear();
                txtLastName.Clear();
                txtStreet.Clear();
                txtCity.Clear();
                txtState.Clear();
                txtZip.Clear();
                txtPhone.Clear();
                txtWeight.Clear();
                txtHeight.Clear();
                txtEmergencyContact.Clear();
                emergencyPhoneTxtBx.Clear();
                txtAptNo.Clear();
                txtUSPAMemNo.Clear();
                DateOfBirth.SelectedDate = new DateTime(1776, 1, 1);
                staffTrueRadBtn.IsChecked = false;
                staffFalseRadBtn.IsChecked = false;
            }
        }

        private void btnUpdateJumper_Click(object sender, RoutedEventArgs e)
        {
            // SHOW THE UPDATE JUMPER TABLE

            Jumper_RMS jumper = new Jumper_RMS();
            jumper.Jumper_ID = jumperIDtxt_UpdateJumper.Content.ToString();
            jumper.First_Name = txtFirstName_UpdateJumper.Text;
            jumper.Last_Name = txtLastName_UpdateJumper.Text;
            jumper.Street = txtStreet_UpdateJumper.Text;
            jumper.Apt_No = txtAptNoUpdateJumper.Text;
            jumper.City = txtCity_UpdateJumper.Text;
            jumper.State = txtState_UpdateJumper.Text;
            jumper.Zip = txtZip_UpdateJumper.Text;
            jumper.Phone_No = txtPhone_UpdateJumper.Text;
            jumper.Weight = txtWeight_UpdateJumper.Text;
            jumper.Height = txtheight_UpdateJumper.Text;
            jumper.Emergency_Contact = txtEmergencyContact_UpdateJumper.Text + "@" + emePhoneTxtBxUpdateJumper.Text;

            try
            {
                jumper.Date_Of_Birth = DateOfBirth_UpdateJumper.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                jumper.Date_Of_Birth = dt.Date;

            }

            string errorCheckingOutput = jumper.ErrorChecking_Update();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            string foreignKeyCheck = jumper.ForeignKey_Check(txtAptNoUpdateJumper.Text);

            if (!jumper.UPSA_MEM_NO.Equals(null) || !jumper.UPSA_MEM_NO.Equals(""))
            {
                if (!foreignKeyCheck.Equals(""))
                {
                    MessageBox.Show(foreignKeyCheck, "Missing Information", MessageBoxButton.OK);
                    return;
                }
            }
  
            else
            {
                if (txtAptNoUpdateJumper.Text == null || txtAptNoUpdateJumper.Text == "")
                {
                    txtAptNoUpdateJumper.Text = null;
                    jumper.Apt_No = txtAptNoUpdateJumper.Text;
                }

                if (txtUSPAMemNoUpdateJumper.Text == null || txtUSPAMemNoUpdateJumper.Text == "")
                {
                    txtUSPAMemNoUpdateJumper.Text = null;
                    jumper.UPSA_MEM_NO = txtUSPAMemNoUpdateJumper.Text;
                }

                //set the appropriate staff attribute for the db - Shawn
                string Staff_UpdateJumper = "";
                if (staffTrueRadBtnUpdate.IsChecked == true)
                {
                    Staff_UpdateJumper = "TRUE";
                    jumper.Staff = Staff_UpdateJumper;
                }
                else
                {
                    Staff_UpdateJumper = "FALSE";
                    jumper.Staff = Staff_UpdateJumper;
                }

                Jumper_DB update_DB = new Jumper_DB();
                update_DB.UpdateJumper(jumper);

                txtFirstName_UpdateJumper.Clear();
                txtLastName_UpdateJumper.Clear();
                txtStreet_UpdateJumper.Clear();
                txtCity_UpdateJumper.Clear();
                txtState_UpdateJumper.Clear();
                txtZip_UpdateJumper.Clear();
                txtPhone_UpdateJumper.Clear();
                txtWeight_UpdateJumper.Clear();
                txtheight_UpdateJumper.Clear();
                txtEmergencyContact_UpdateJumper.Clear();
                emePhoneTxtBxUpdateJumper.Clear();
                txtAptNoUpdateJumper.Clear();
                txtUSPAMemNoUpdateJumper.Clear();
                DateOfBirth_UpdateJumper.SelectedDate = new DateTime(1776, 1, 1);
                staffTrueRadBtnUpdate.IsChecked = false;
                staffFalseRadBtnUpdate.IsChecked = false;
            }
        }

        private void btnDeleteJumper_Click(object sender, RoutedEventArgs e)
        {
            // ADMIN PERMISSION REQUIRED TO ACCESS THIS FORM
            // SHOW DELETE JUMPER FORM ONCE ADMIN CREDENTIALS ACCEPTED
            Jumper_RMS jumper = new Jumper_RMS();
            jumper.Jumper_ID = jumper_id_txtbx_delete.Text;
            
            if (jumper.Jumper_ID == "" || jumper.Jumper_ID == null)
            {
                MessageBox.Show("A Jumper_ID must be entered in order for this to function correctly.", "Missing Jumper ID", MessageBoxButton.OK);
            }
            else
            {
                Jumper_DB deleteJumperDB = new Jumper_DB();
                deleteJumperDB.DeleteJumper(jumper);

                jumper_id_txtbx_delete.Clear();
            }
        }

        private void searchJumperBtn_Click(object sender, RoutedEventArgs e)
        {
            Jumper_RMS jumper = new Jumper_RMS();
            jumper.Jumper_ID = jumper_id_txtbx_search.Text;

            if (jumper.Jumper_ID == null || jumper.Jumper_ID == "")
            {
                MessageBox.Show("A Jumper_ID must be entered in order for this to function correctly.", "Missing Jumper ID", MessageBoxButton.OK);
            }
            else
            {
                Jumper_DB searchJumperDB = new Jumper_DB();
                DataTable dt = searchJumperDB.retrieveJumperForDisplay(jumper);
                dataGridJumper.ItemsSource = dt.DefaultView;

                jumper_id_txtbx_search.Clear();
            }
        }

        private void allJumpersBtn_Click(object sender, RoutedEventArgs e)
        {
            Jumper_DB allJumpers = new Jumper_DB();
            DataTable dt = allJumpers.allJumpers();
            dataGridJumper.ItemsSource = dt.DefaultView;
        }

        private void customersBtn_Click(object sender, RoutedEventArgs e)
        {
            Jumper_DB allCustomers = new Jumper_DB();
            DataTable dt = allCustomers.allCustomers();
            dataGridJumper.ItemsSource = dt.DefaultView;
        }

        private void allEmployeesBtn_Click(object sender, RoutedEventArgs e)
        {
            Jumper_DB allEmployees = new Jumper_DB();
            DataTable dt = allEmployees.allEmployees();
            dataGridJumper.ItemsSource = dt.DefaultView;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

            switch (tabItem)
            {
                //case "Item1":
                //break;

                case "Update Jumper":
                    //input box is the name of the code at end of grid
                    //UpdateInputBox.Visibility = Visibility.Visible;
                    /*Update_Jumper_ID_DialogWindow ujid = new Update_Jumper_ID_DialogWindow();
                    ujid.ShowDialog();
                    string jumper_ID_For_Update = "";
                    jumper_ID_For_Update = ujid.jumper_ID;
                    ujid.Close();*/

                    Update_Window uw = new Update_Window();
                    uw.Table_ID_Lbl.Content = "Jumper ID:";

                    try
                    {
                        uw.ShowDialog();
                    }
                    catch(Exception excp)
                    {
                        return;
                    }
                    

                    string jumper_ID_For_Update = "";
                    jumper_ID_For_Update = uw.ID_Return;
                    uw.Close();

                    if (jumper_ID_For_Update == null || jumper_ID_For_Update == "")
                    {
                        return;
                    }
                    else
                    {
                        Jumper_RMS jumper = new Jumper_RMS();
                        jumper.Jumper_ID = jumper_ID_For_Update;
                        Jumper_DB ret_db = new Jumper_DB();
                        List<string> retrievedJumperInfo = ret_db.retrieveJumperForUpdate(jumper);

                        try
                        {
                            jumperIDtxt_UpdateJumper.Content = retrievedJumperInfo[0];
                            txtFirstName_UpdateJumper.Text = retrievedJumperInfo[1];
                            txtLastName_UpdateJumper.Text = retrievedJumperInfo[2];
                            txtStreet_UpdateJumper.Text = retrievedJumperInfo[3];
                            txtAptNoUpdateJumper.Text = retrievedJumperInfo[4];
                            txtCity_UpdateJumper.Text = retrievedJumperInfo[5];
                            txtState_UpdateJumper.Text = retrievedJumperInfo[6];
                            txtZip_UpdateJumper.Text = retrievedJumperInfo[7];
                            DateOfBirth_UpdateJumper.Text = retrievedJumperInfo[8];
                            txtPhone_UpdateJumper.Text = retrievedJumperInfo[9];
                            txtWeight_UpdateJumper.Text = retrievedJumperInfo[10];
                            txtheight_UpdateJumper.Text = retrievedJumperInfo[11];
                            //txtEmergencyContact_UpdateJumper.Text = retrievedJumperInfo[12];
                            //txtemerphone_UpdateJumper.Text = retrievedJumperInfo[13];
                            string fullEmeContact = retrievedJumperInfo[12];
                            List<string> abc = fullEmeContact.Split('@').ToList();
                            txtEmergencyContact_UpdateJumper.Text = abc[0];
                            emePhoneTxtBxUpdateJumper.Text = abc[1];
                            string staff = retrievedJumperInfo[13];
                            if (staff == "TRUE" || staff == "True")
                            {
                                staffTrueRadBtnUpdate.IsChecked = true;
                                staffFalseRadBtnUpdate.IsChecked = false;
                            }
                            else
                            {
                                staffFalseRadBtnUpdate.IsChecked = true;
                                staffTrueRadBtnUpdate.IsChecked = false;
                            }
                            txtUSPAMemNoUpdateJumper.Text = retrievedJumperInfo[14];
                        }
                        catch(Exception excp)
                        {
                            MessageBox.Show("The requested data could not be retrieved.", "Error", MessageBoxButton.OK);
                        }

                    }

                    return;

                default:
                    return;
            }
        }
    }
}
