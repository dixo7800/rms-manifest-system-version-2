﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Jump_Window.xaml
    /// </summary>
    public partial class Jump_Window : Window
    {
        public Jump_Window()
        {
            InitializeComponent();
            this.Closed += Jump_Window_Closed;
        }

        private void Jump_Window_Closed(object sender, EventArgs e)
        {
            Home_Window home = new Home_Window();
            home.Show();
            this.Close();
        }

        private void btnUpdateJump_Click(object sender, RoutedEventArgs e)
        {
            Jump_RMS jump = new Jump_RMS();
            jump.Jump_ID = jumpIDtxt_UpdateJump.Content.ToString();

            try
            {
                jump.Jump_Date = JumpDate_UpdateJump.SelectedDate.Value.Date;
            }
            catch(Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                jump.Load_Date = dt.Date;
            }
            
            jump.Style = Style_TxtBx.Text;
            jump.Jumper_ID = JumperID_Update_TxtBx.Text;
            jump.Load_NO = txtLoad_UpdateJump.Text;

            try
            {
                jump.Load_Date = LoadDate_UpdateJump.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                jump.Load_Date = dt.Date;

            }

            jump.Plane_ID = txtPlaneID_UpdateJump.Text;
            jump.Cost_Code = txtCostCode_UpdateJump.Text;



            string errorCheckingOutput = jump.ErrorChecking_Update();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            string foreignKeyCheck = jump.ForeignKey_Check(jump.Jumper_ID, jump.Load_NO, jump.Load_Date, jump.Plane_ID, jump.Cost_Code);

           
            if (!foreignKeyCheck.Equals(""))
            {
                MessageBox.Show(foreignKeyCheck, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                if (jump.Jumper_ID == null || jump.Jumper_ID == "")
                {
                    jump.Jumper_ID = null;
                }

                if (jump.Load_NO == null || jump.Load_NO == "")
                {
                    jump.Load_NO = null;
                }
                
                if (jump.Plane_ID == null || jump.Plane_ID == "")
                {
                    jump.Plane_ID = null;
                }

                if (jump.Cost_Code == null || jump.Cost_Code == "")
                {
                    jump.Cost_Code = null;
                }

                Jump_DB update_DB = new Jump_DB();
                update_DB.Update_Jumps(jump);

                jumpIDtxt_UpdateJump.Content = "";
                JumpDate_UpdateJump.SelectedDate = new DateTime(1776, 1, 1);
                Style_TxtBx.Clear();
                JumperID_Update_TxtBx.Clear();
                txtLoad_UpdateJump.Clear();
                LoadDate_UpdateJump.SelectedDate = new DateTime(1776, 1, 1);
                txtPlaneID_UpdateJump.Clear();
                txtCostCode_UpdateJump.Clear();
            }
        }

        private void btnDeleteJump_Click(object sender, RoutedEventArgs e)
        {
            Jump_RMS jump = new Jump_RMS();
            jump.Jump_ID = jump_id_txtbx_delete.Text;

            if (jump.Jump_ID == null || jump.Jump_ID == "")
            {
                MessageBox.Show("A Jump ID must be entered in order for this to function correctly.", "Missing Jumper ID", MessageBoxButton.OK);
            }
            else
            {
                Jump_DB deleteJumpDB = new Jump_DB();
                deleteJumpDB.Delete_Jumps(jump);

                jump_id_txtbx_delete.Clear();
            }
        }

        private void searchJumpBtn_Click(object sender, RoutedEventArgs e)
        {
            Jump_RMS jump = new Jump_RMS();
            jump.Jump_ID = jump_id_txtbx_search.Text;

            if (jump.Jump_ID == null || jump.Jump_ID == "")
            {
                MessageBox.Show("A Jump ID must be entered in order for this to function correctly.", "Missing Jumper ID", MessageBoxButton.OK);
            }
            else
            {
                Jump_DB searchJumpDB = new Jump_DB();
                DataTable dt = searchJumpDB.Retrieve_JumpsForDisplay(jump);
                dataGridJumps.ItemsSource = dt.DefaultView;

                jump_id_txtbx_search.Clear();
            }
        }

        private void allJumpBtn_Click(object sender, RoutedEventArgs e)
        {
            try {
                Jump_DB allJumps = new Jump_DB();
                DataTable dt = allJumps.All_Jumps();
                dataGridJumps.ItemsSource = dt.DefaultView;
            }
            catch(Exception e1)
            {
                Console.WriteLine("Error: " + e1);
                this.Close();
            }
        }
        
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

            switch (tabItem)
            {
                case "Update Jump":
                    Update_Window uw = new Update_Window();
                    uw.Table_ID_Lbl.Content = "Jump ID:";

                    try
                    {
                        uw.ShowDialog();
                    }
                    catch(Exception excp)
                    {
                        return;
                    }

                    string Jump_ID_Update = "";
                    Jump_ID_Update = uw.ID_Return;
                    uw.Close();

                    if (Jump_ID_Update == null || Jump_ID_Update == "")
                    {
                        return;
                    }
                    else
                    {
                        Jump_RMS jump = new Jump_RMS();
                        jump.Jump_ID = Jump_ID_Update;
                        Jump_DB ret_DB = new Jump_DB();
                        List<string> retrievedCostUpdate = ret_DB.Retrieve_JumpsForUpdate(jump);

                        try
                        {
                            jumpIDtxt_UpdateJump.Content = retrievedCostUpdate[0];
                            JumpDate_UpdateJump.Text = retrievedCostUpdate[1];
                            Style_TxtBx.Text = retrievedCostUpdate[2];
                            JumperID_Update_TxtBx.Text = retrievedCostUpdate[3];
                            txtLoad_UpdateJump.Text = retrievedCostUpdate[4];
                            LoadDate_UpdateJump.Text = retrievedCostUpdate[5];
                            txtPlaneID_UpdateJump.Text = retrievedCostUpdate[6];
                            txtCostCode_UpdateJump.Text = retrievedCostUpdate[7];
                        }
                        catch (Exception excp)
                        {
                            MessageBox.Show("The requested data could not be retrieved.","Error",MessageBoxButton.OK);
                        }

                    }

                    return;

                default:
                    return;
            }
        }
    }
}
