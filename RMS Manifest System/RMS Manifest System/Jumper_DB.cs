﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Jumper_DB:Database
    {
        public DataTable allJumpers()
        {
            string storedProcedureName = "ALL_JUMPERS";
            string dataTableName = "Jumpers";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        //need to double check the varaible types, and see if they match the variables in the database
        public void AddJumper(Jumper_RMS jumper)
        {
            object var1 = null;
            object var2 = null;
            if (jumper.Apt_No == null || jumper.Apt_No == "")
            {
                var1 = DBNull.Value;
                //Apt_NO = DBNull.Value;
            }
            else
            {
                var1 = jumper.Apt_No;
            }

            if (jumper.UPSA_MEM_NO == null || jumper.UPSA_MEM_NO == "")
            {
                var2 = DBNull.Value;
            }
            else
            {
                var2 = jumper.UPSA_MEM_NO;
            }

            string storedProcedureName = "Insert_New_Jumper";
            List<string> storedProcedureVariables = new List<string>() { "@First_Name", "@Last_Name", "@Street", "@Apt#", "@City", "@State", "@Zip", "@DOB", "@phone", "@weight", "@height", "@emergency_contact", "@staff", "@uspa_mem_no" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { jumper.First_Name, jumper.Last_Name, jumper.Street, var1, jumper.City, jumper.State, jumper.Zip, jumper.Date_Of_Birth, jumper.Phone_No, jumper.Weight, jumper.Height, jumper.Emergency_Contact, jumper.Staff, var2 };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        //need to see if the for loops return what I think they do, Im sure it will but it should be checked
        public List<string> retrieveJumperForUpdate(Jumper_RMS jumper)
        {
            string storedProcedureName = "Retrieve_Jumper";
            string dataTableName = "Jumper " + jumper.Jumper_ID;
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { jumper.Jumper_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> jumperInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    jumperInfo.Add(item.ToString());
                }

            }

            return jumperInfo;
        }

        public DataTable retrieveJumperForDisplay(Jumper_RMS jumper)
        {
            string storedProcedureName = "Retrieve_Jumper";
            string dataTableName = "Jumper " + jumper.Jumper_ID;
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { jumper.Jumper_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        //need to return all data first, then we will load form with current data, changes will be made, then we will reload all the data back into the database
        //need to check and talk about variable types
        public void UpdateJumper(Jumper_RMS jumper)
        {
            object var1 = null;
            object var2 = null;
            if (jumper.Apt_No == null || jumper.Apt_No == "")
            {
                var1 = DBNull.Value;
                //Apt_NO = DBNull.Value;
            }
            else
            {
                var1 = jumper.Apt_No;
            }

            if (jumper.UPSA_MEM_NO == null || jumper.UPSA_MEM_NO == "")
            {
                var2 = DBNull.Value;
            }
            else
            {
                var2 = jumper.UPSA_MEM_NO;
            }

            string storedProcedureName = "Update_Jumper";

            List<string> storedProcedureVariables = new List<string>() { "@Jumper_ID", "@First_Name", "@Last_Name", "@Street", "@Apt#", "@City", "@State", "@Zip", "@DOB", "@phone", "@weight", "@height", "@emergency_contact", "@staff", "@uspa_mem_no" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.DateTime, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { jumper.Jumper_ID, jumper.First_Name, jumper.Last_Name, jumper.Street, var1, jumper.City, jumper.State, jumper.Zip, jumper.Date_Of_Birth, jumper.Phone_No, jumper.Weight, jumper.Height, jumper.Emergency_Contact, jumper.Staff, var2 };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void DeleteJumper(Jumper_RMS jumper)
        {
            string storedProcedureName = "Delete_Jumper";
            List<string> storedProcedureVariables = new List<string>() { "@Jumper_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { jumper.Jumper_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);

        }

        //still need a function to grab just the customers
        public DataTable allCustomers()
        {
            string storedProcedureName = "All_Customers";
            string dataTableName = "Customers";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        //still need a function to grab just the employees
        public DataTable allEmployees()
        {
            string storedProcedureName = "All_Employees";
            string dataTableName = "Employees";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        //what is this for?
        public string jumper_id_for_update = "";

        public string JUMPER_ID_Lookup(string JUMPER_ID)
        {
            string result = "";

            string storedProcedureName = "JUMPER_ID_Lookup";
            string dataTableName = "JUMPER_ID";
            List<string> storedProcedureVariables = new List<string>() { "@JUMPER_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { JUMPER_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            if (dt == null)
            {
                result = "";
                return result;
            }

            List<string> jumper_IDs = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    jumper_IDs.Add(item.ToString());
                }

            }

            if (jumper_IDs.Count == 0)
            {
                result = "";
                return result;
            }

            result = jumper_IDs[0];

            return result;
        }
    }
}
