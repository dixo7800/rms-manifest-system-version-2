﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Rig_Window.xaml
    /// </summary>
    public partial class Rig_Window : Window
    {
        public Rig_Window()
        {
            InitializeComponent();
            this.Closed += Rig_Window_Closed;
        }

        private void Rig_Window_Closed(object sender, EventArgs e)
        {
            Home_Window home = new Home_Window();
            home.Show();
            this.Close();
        }

        private void btnAddRig_Click(object sender, RoutedEventArgs e)
        {
            Rig_RMS rig = new Rig_RMS();
            rig.Container_Type = CType_txtbx.Text;
            rig.Container_Colors = CColor_txtbx.Text;
            rig.Main_Canopy = MainCanopy_txtbx.Text;
            rig.Main_Colors = MainColor_txtbx.Text;
            rig.Reserve_Canopy = ReserveCanopy_txtbx.Text;

            try
            {
                rig.Reserve_Repack_Date = ReserveRepackDate_DatePicker.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                rig.Reserve_Repack_Date = dt.Date;

            }

            rig.AAD_Type = AADType_txtbx.Text;
            rig.Tandem = Tandem_txtbx.Text;
            rig.Rental = Rental_txtbx.Text;
            rig.Jumper_ID = JumperID_txtbx.Text;

            string errorCheckingOutput = rig.ErrorChecking_Add();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            string foreignKeyCheck = rig.ForeignKey_Check(rig.Jumper_ID);


            if (!foreignKeyCheck.Equals(""))
            {
                MessageBox.Show(foreignKeyCheck, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                if (rig.Jumper_ID == null || rig.Jumper_ID == "")
                {
                    rig.Jumper_ID = null;
                }

                Rig_DB add_DB = new Rig_DB();
                add_DB.Insert_New_Rig(rig);

                CType_txtbx.Clear();
                CColor_txtbx.Clear();
                MainCanopy_txtbx.Clear();
                MainColor_txtbx.Clear();
                ReserveCanopy_txtbx.Clear();
                ReserveRepackDate_DatePicker.SelectedDate = new DateTime(1776, 1, 1);
                AADType_txtbx.Clear();
                Tandem_txtbx.Clear();
                Rental_txtbx.Clear();
                JumperID_txtbx.Clear();
            }
        }

        private void btnUpdateRig_Click(object sender, RoutedEventArgs e)
        {
            Rig_RMS rig = new Rig_RMS();
            rig.Rig_ID = RigID_Update_Lbl.Content.ToString();
            rig.Container_Type = CType_txtbx_UpdateRig.Text;
            rig.Container_Colors = CColor_txtbx_UpdateRig.Text;
            rig.Main_Canopy = MainCanopy_txtbx_UpdateRig.Text;
            rig.Main_Colors = MainColor_txtbx_UpdateRig.Text;
            rig.Reserve_Canopy = ReserveCanopy_txtbx_UpdateRig.Text;

            try
            {
                rig.Reserve_Repack_Date = ReserveRepackDate_datepicker_UpdateRig.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                rig.Reserve_Repack_Date = dt.Date;

            }

            rig.AAD_Type = AADType_txtbx_UpdateRig.Text;
            rig.Tandem = Tandem_txtbx_UpdateRig.Text;
            rig.Rental = Rental_txtbx_UpdateRig.Text;
            rig.Jumper_ID = JumperID_Update_TxtBx.Text;

            string errorCheckingOutput = rig.ErrorChecking_Update();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            string foreignKeyCheck = rig.ForeignKey_Check(rig.Jumper_ID);


            if (!foreignKeyCheck.Equals(""))
            {
                MessageBox.Show(foreignKeyCheck, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                if (rig.Jumper_ID == null || rig.Jumper_ID == "")
                {
                    rig.Jumper_ID = null;
                }

                Rig_DB update_DB = new Rig_DB();
                update_DB.Update_Rig(rig);

                RigID_Update_Lbl.Content = "";
                CType_txtbx_UpdateRig.Clear();
                CColor_txtbx_UpdateRig.Clear();
                MainCanopy_txtbx_UpdateRig.Clear();
                MainColor_txtbx_UpdateRig.Clear();
                ReserveCanopy_txtbx_UpdateRig.Clear();
                ReserveRepackDate_datepicker_UpdateRig.SelectedDate = new DateTime(1776, 1, 1);
                AADType_txtbx_UpdateRig.Clear();
                Tandem_txtbx_UpdateRig.Clear();
                Rental_txtbx_UpdateRig.Clear();
                JumperID_Update_TxtBx.Clear();
            }
        }

        private void btnDeleteRig_Click(object sender, RoutedEventArgs e)
        {
            Rig_RMS rig = new Rig_RMS();
            rig.Rig_ID = RigID_Delete_TxtBx.Text;

            if (rig.Rig_ID == null || rig.Rig_ID == "")
            {
                MessageBox.Show("A Rig ID must be entered in order for this to function correctly.", "Missing Rig ID", MessageBoxButton.OK);
            }
            else
            {
                Rig_DB deleteRigDB = new Rig_DB();
                deleteRigDB.Delete_Rig(rig);

                RigID_Delete_TxtBx.Clear();
            }
        }

        private void btnSearchRig_Click(object sender, RoutedEventArgs e)
        {
            Rig_RMS rig = new Rig_RMS();
            rig.Rig_ID = RigID_txtbx_SearchRig.Text;

            if (rig.Rig_ID == null || rig.Rig_ID == "")
            {
                MessageBox.Show("A Rig ID must be entered in order for this to function correctly.", "Missing Rig ID", MessageBoxButton.OK);
            }
            else
            {
                Rig_DB searchRigDB = new Rig_DB();
                DataTable dt = searchRigDB.Retrieve_RigForDisplay(rig);
                DataGrid_Rig.ItemsSource = dt.DefaultView;

                RigID_txtbx_SearchRig.Clear();
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

            switch (tabItem)
            {
                case "Update Rig":
                    Update_Window uw = new Update_Window();
                    uw.Table_ID_Lbl.Content = "Rig ID:";

                    try
                    {
                        uw.ShowDialog();
                    }
                    catch (Exception excp)
                    {
                        return;
                    }

                    string Rig_ID_Update = "";
                    Rig_ID_Update = uw.ID_Return;
                    uw.Close();

                    if (Rig_ID_Update == null || Rig_ID_Update == "")
                    {
                        return;
                    }
                    else
                    {
                        Rig_RMS rig = new Rig_RMS();
                        rig.Rig_ID = Rig_ID_Update;
                        Rig_DB ret_DB = new Rig_DB();
                        List<string> retrievedRigUpdate = ret_DB.Retrieve_RigForUpdate(rig);

                        try
                        {
                            RigID_Update_Lbl.Content = retrievedRigUpdate[0];
                            CType_txtbx_UpdateRig.Text = retrievedRigUpdate[1];
                            CColor_txtbx_UpdateRig.Text = retrievedRigUpdate[2];
                            MainCanopy_txtbx_UpdateRig.Text = retrievedRigUpdate[3];
                            MainColor_txtbx_UpdateRig.Text = retrievedRigUpdate[4];
                            ReserveCanopy_txtbx_UpdateRig.Text = retrievedRigUpdate[5];
                            ReserveRepackDate_datepicker_UpdateRig.Text = retrievedRigUpdate[6];
                            AADType_txtbx_UpdateRig.Text = retrievedRigUpdate[7];
                            Tandem_txtbx_UpdateRig.Text = retrievedRigUpdate[8];
                            Rental_txtbx_UpdateRig.Text = retrievedRigUpdate[9];
                            JumperID_Update_TxtBx.Text = retrievedRigUpdate[10];
                        }
                        catch (Exception excp)
                        {
                            MessageBox.Show("The requested data could not be retrieved.", "Error", MessageBoxButton.OK);
                        }

                    }

                    return;

                default:
                    return;
            }
        }
    }
}
