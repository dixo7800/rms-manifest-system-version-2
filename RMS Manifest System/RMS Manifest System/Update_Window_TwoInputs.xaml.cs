﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Update_Window_TwoInputs.xaml
    /// </summary>
    public partial class Update_Window_TwoInputs : Window
    {
        public Update_Window_TwoInputs()
        {
            InitializeComponent();
        }

        private void Submit_Btn_Click(object sender, RoutedEventArgs e)
        {
            ID_Return = Table_ID_TxtBx.Text;
            Date_Return = Table_Date_DtPk.SelectedDate.Value.Date;
            this.Hide();
        }

        private void Cancel_Btn_Click(object sender, RoutedEventArgs e)
        {
            Table_ID_TxtBx.Text = "";
            ID_Return = Table_ID_TxtBx.Text;
            DateTime dt = new DateTime(1776, 1, 1);
            Date_Return = dt.Date;
            this.Hide();
        }

        public string ID_Return;

        public DateTime Date_Return;
    }
}
