﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class License_RMS
    {
        public string USPA_MEM_NO { get; set; }
        public string USPA_LIC_NO { get; set; }
        public DateTime USPA_EXP_Date { get; set; }

        public string ErrorChecking()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("USPA Member Number", USPA_MEM_NO);
            dict.Add("USPA License Number", USPA_LIC_NO);
            dict.Add("USPA Expiration Date", USPA_EXP_Date);


            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "" || d.Value.ToString().Contains("1/1/1776"))
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }

        public string USPA_MEM_NO_CHECK(string USPA_ID)
        {
            string message = "";

            License_DB lic = new License_DB();
            string value = lic.USPA_MEM_NO_Lookup(USPA_ID);

            if (!value.Equals(""))
            {
                message = " The USPA_MEMBER_NUMBER that was referenced, " + value + " is already in the database. ";
            }
            else
            {
                message = "";
            }

            return message;
        }
    }
}
