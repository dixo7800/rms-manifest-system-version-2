﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Rig_RMS
    {
        public string Rig_ID { get; set; }
        public string Container_Type { get; set; }
        public string Container_Colors { get; set; }
        public string Main_Canopy { get; set; }
        public string Main_Colors { get; set; }
        public string Reserve_Canopy { get; set; }
        public DateTime Reserve_Repack_Date { get; set; }
        public string AAD_Type { get; set; }
        public string Tandem { get; set; }
        public string Rental { get; set; }
        public string Jumper_ID { get; set; }

        public string ErrorChecking_Add()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("Container Type", Container_Type);
            dict.Add("Container Colors", Container_Colors);
            dict.Add("Main Canopy", Main_Canopy);
            dict.Add("Main Colors", Main_Colors);
            dict.Add("Reserve Canopy", Reserve_Canopy);
            dict.Add("Reserve Repack Date", Reserve_Repack_Date);
            dict.Add("AAD Type", AAD_Type);
            dict.Add("Tandem", Tandem);
            dict.Add("Rental", Rental);
            dict.Add("Jumper ID", Jumper_ID);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "" || d.Value.ToString().Contains("1/1/1776"))
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }

        public string ErrorChecking_Update()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("Rig ID", Rig_ID);
            dict.Add("Container Type", Container_Type);
            dict.Add("Container Colors", Container_Colors);
            dict.Add("Main Canopy", Main_Canopy);
            dict.Add("Main Colors", Main_Colors);
            dict.Add("Reserve Canopy", Reserve_Canopy);
            dict.Add("Reserve Repack Date", Reserve_Repack_Date);
            dict.Add("AAD Type", AAD_Type);
            dict.Add("Tandem", Tandem);
            dict.Add("Rental", Rental);
            dict.Add("Jumper ID", Jumper_ID);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "" || d.Value.ToString().Contains("1/1/1776"))
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }

        private string ForeignKeyCheck_Jumper_ID (string jumper_id)
        {
            string message = "";


            Jumper_DB jumper = new Jumper_DB();
            string value = jumper.JUMPER_ID_Lookup(jumper_id);

            if (!value.Equals(""))
            {
                message = "";
            }
            else
            {
                message = " The Jumper ID that was referenced is not in the database. ";
            }


            return message;
        }

        public string ForeignKey_Check(string jumper_id)
        {
            string message = "";

            string a = ForeignKeyCheck_Jumper_ID(jumper_id);

            message = message + a;

            return message;
        }
    }
}
