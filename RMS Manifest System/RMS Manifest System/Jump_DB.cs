﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Jump_DB:Database
    {
        public DataTable All_Jumps()
        {
            string storedProcedureName = "ALL_Jump";
            string dataTableName = "Jumps";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);
            return dt;
        }

        public void Insert_New_Jumps(Jump_RMS jump)
        {
            object var1 = null;
            object var2 = null;
            object var3 = null;
            object var4 = null;

            if(jump.Jumper_ID == null || jump.Jumper_ID == "")
            {
                var1 = DBNull.Value;
            }
            else
            {
                var1 = jump.Jumper_ID;
            }

            if (jump.Load_NO == null || jump.Load_NO == "")
            {
                var2 = DBNull.Value;
            }
            else
            {
                var2 = jump.Load_NO;
            }

            if (jump.Plane_ID == null || jump.Plane_ID == "")
            {
                var3 = DBNull.Value;
            }
            else
            {
                var3 = jump.Plane_ID;
            }

            if (jump.Cost_Code == null || jump.Cost_Code == "")
            {
                var4 = DBNull.Value;
            }
            else
            {
                var4 = jump.Cost_Code;
            }


            string storedProcedureName = "Insert_New_Jump";
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_DATE", "@STYLE", "@JUMPER_ID", "@LOAD_NO", "@LOAD_DATE", "@PLANE_ID", "@COST_CODE" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { jump.Jump_Date, jump.Style, var1, var2, jump.Load_Date, var3, var4 };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Update_Jumps(Jump_RMS jump)
        {
            object var1 = null;
            object var2 = null;
            object var3 = null;
            object var4 = null;

            if (jump.Jumper_ID == null || jump.Jumper_ID == "")
            {
                var1 = DBNull.Value;
            }
            else
            {
                var1 = jump.Jumper_ID;
            }

            if (jump.Load_NO == null || jump.Load_NO == "")
            {
                var2 = DBNull.Value;
            }
            else
            {
                var2 = jump.Load_NO;
            }

            if (jump.Plane_ID == null || jump.Plane_ID == "")
            {
                var3 = DBNull.Value;
            }
            else
            {
                var3 = jump.Plane_ID;
            }

            if (jump.Cost_Code == null || jump.Cost_Code == "")
            {
                var4 = DBNull.Value;
            }
            else
            {
                var4 = jump.Cost_Code;
            }

            string storedProcedureName = "Update_Jump";
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_ID", "@JUMP_DATE", "@STYLE", "@JUMPER_ID", "@LOAD_NO", "@LOAD_DATE", "@PLANE_ID", "@COST_CODE" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { jump.Jump_ID, jump.Jump_Date, jump.Style, var1, var2, jump.Load_Date, var3, var4 };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_Jumps(Jump_RMS jump)
        {
            string storedProcedureName = "Delete_Jump";
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { jump.Jump_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public List<string> Retrieve_JumpsForUpdate(Jump_RMS jump)
        {
            string storedProcedureName = "Retrieve_Jump";
            string dataTableName = "Jumps";
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { jump.Jump_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> loadInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    loadInfo.Add(item.ToString());
                }

            }

            return loadInfo;
        }

        public DataTable Retrieve_JumpsForDisplay(Jump_RMS jump)
        {
            string storedProcedureName = "Retrieve_Jump";
            string dataTableName = "Jumps";
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { jump.Jump_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }
    }
}
