﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Jump_RMS
    {
        public string Jump_ID { get; set; }
        public DateTime Jump_Date { get; set; }
        public string Style { get; set; }
        public string Jumper_ID { get; set; }
        public string Load_NO { get; set; }
        public DateTime Load_Date { get; set; }
        public string Plane_ID { get; set; }
        public string Cost_Code { get; set; }

        public string ErrorChecking_Add()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("Jump Date", Jump_Date);
            dict.Add("Style", Style);
            dict.Add("Jumper ID", Jumper_ID);
            dict.Add("Load Number", Load_NO);
            dict.Add("Load Date", Load_Date);
            dict.Add("Plane ID", Plane_ID);
            dict.Add("Cost Code", Cost_Code);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "" || d.Value.ToString().Contains("1/1/1776"))
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }

        public string ErrorChecking_Update()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("Jump ID", Jump_ID);
            dict.Add("Jump Date", Jump_Date);
            dict.Add("Style", Style);
            dict.Add("Jumper ID", Jumper_ID);
            dict.Add("Load Number", Load_NO);
            dict.Add("Load Date", Load_Date);
            dict.Add("Plane ID", Plane_ID);
            dict.Add("Cost Code", Cost_Code);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "" || d.Value.ToString().Contains("1/1/1776"))
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }

        private string ForeignKeyCheck_Jumper_ID(string jumper_id)
        {
            string message = "";


            Jumper_DB jumper = new Jumper_DB();
            string value = jumper.JUMPER_ID_Lookup(jumper_id);

            if (!value.Equals(""))
            {
                message = "";
            }
            else
            {
                message = " The Jumper ID that was referenced is not in the database. ";
            }


            return message;
        }

        private string ForeignKeyCheck_LoadNO_LoadDate(string load_no, DateTime loadDate)
        {
            string message = "";

            Load_DB load = new Load_DB();
            string value = load.LOAD_NO_LOAD_DATE_Lookup(load_no, loadDate);

            if (!value.Equals(""))
            {
                message = "";
            }
            else
            {
                message = " The Load Number and/or the Load Date that was referenced is not in the database. ";
            }

            return message;
        }

        private string ForeignKeyCheck_Plane_ID(string plane_id)
        {
            string message = "";

            Plane_DB plane = new Plane_DB();
            string value = plane.PLANE_ID_Lookup(plane_id);

            if (!value.Equals(""))
            {
                message = "";
            }
            else
            {
                message = " The Plane ID that was referenced is not in the database. ";
            }

            return message;
        }

        private string ForeignKeyCheck_Cost_Code(string cost_code)
        {
            string message = "";

            Cost_DB cost = new Cost_DB();
            string value = cost.COST_CODE_Lookup(cost_code);

            if (!value.Equals(""))
            {
                message = "";
            }
            else
            {
                message = " The Cost Code that was referenced is not in the database. ";
            }

            return message;
        }

        public string ForeignKey_Check(string jumper_id, string load_no, DateTime load_date, string plane_id, string cost_code)
        {
            string message = "";

            string a = ForeignKeyCheck_Jumper_ID(jumper_id);

            string b = ForeignKeyCheck_LoadNO_LoadDate(load_no, load_date);

            string c = ForeignKeyCheck_Plane_ID(plane_id);

            string d = ForeignKeyCheck_Cost_Code(cost_code);

            message = message + a + b + c + d;

            return message;
        }
    }
}
