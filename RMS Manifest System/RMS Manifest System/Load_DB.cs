﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Load_DB:Database
    {
        public DataTable All_Loads()
        {
            string storedProcedureName = "ALL_Loads";
            string dataTableName = "Loads";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);
            return dt;
        }

        public void Insert_New_Load(Load_RMS load)
        {
            string storedProcedureName = "Insert_New_Loads";
            List<string> storedProcedureVariables = new List<string>() { "@load_date", "@pilot", "@fuel_load" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { load.Load_Date, load.Pilot, load.Fuel_Load };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Update_Load(Load_RMS load)
        {
            string storedProcedureName = "Update_Loads";
            List<string> storedProcedureVariables = new List<string>() { "@load_no", "@load_date", "@pilot", "@fuel_load" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { load.Load_NO, load.Load_Date, load.Pilot, load.Fuel_Load };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_Load(Load_RMS load)
        {
            string storedProcedureName = "Delete_Loads";
            List<string> storedProcedureVariables = new List<string>() { "@load_no", "@load_date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date };
            List<object> Values = new List<object>() { load.Load_NO, load.Load_Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public List<string> Retrieve_LoadForUpdate(Load_RMS load)
        {
            string storedProcedureName = "Retrieve_Loads";
            string dataTableName = "Loads";
            List<string> storedProcedureVariables = new List<string>() { "@load_no", "@load_date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date };
            List<object> Values = new List<object>() { load.Load_NO, load.Load_Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> loadInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    loadInfo.Add(item.ToString());
                }

            }

            return loadInfo;
        }

        public DataTable Retrieve_LoadForDisplay(Load_RMS load)
        {
            string storedProcedureName = "Retrieve_Loads";
            string dataTableName = "Loads";
            List<string> storedProcedureVariables = new List<string>() { "@load_no", "@load_date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date };
            List<object> Values = new List<object>() { load.Load_NO, load.Load_Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public string LOAD_NO_LOAD_DATE_Lookup(string Load_No, DateTime Load_Date)
        {
            string result = "";

            string storedProcedureName = "LOAD_NO_LOAD_DATE_Lookup";
            string dataTableName = "Loads PK Info";
            List<string> storedProcedureVariables = new List<string>() { "@LOAD_NO", "@LOAD_DATE" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date };
            List<object> Values = new List<object>() { Load_No, Load_Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            if (dt == null)
            {
                result = "";
                return result;
            }

            List<string> loads_pk = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    loads_pk.Add(item.ToString());
                }

            }

            if (loads_pk.Count == 0)
            {
                result = "";
                return result;
            }

            result = loads_pk[0];

            return result;
        }
    }
}
