﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Has_Rating_DB:Database
    {
        public DataTable All_Has_Ratings()
        {
            string storedProcedureName = "All_Has_Rating";
            string dataTableName = "Has Rating";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public void Insert_New_Has_Rating(Has_Rating_RMS hs)
        {
            string storedProcedureName = "Insert_Has_Rating";
            List<string> storedProcedureVariables = new List<string>() { "@USPA_MEM_NO", "@RATING_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { hs.USPA_MEM_NO, hs.Rating_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_Has_Rating(Has_Rating_RMS hs)
        {
            string storedProcedureName = "Delete_Has_Rating";
            List<string> storedProcedureVariables = new List<string>() { "@USPA_MEMBER_NO", "@RATING_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { hs.USPA_MEM_NO, hs.Rating_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public DataTable Retrieve_USPA_MEM_Has_Rating_ForDisplay(Has_Rating_RMS hs)
        {
            string storedProcedureName = "Retrieve_All_USPA_MEM_Has_Rating";
            string dataTableName = "Has Rating";
            List<string> storedProcedureVariables = new List<string>() { "@USPA_MEMBER_NO" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { hs.USPA_MEM_NO };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }
    }
}
