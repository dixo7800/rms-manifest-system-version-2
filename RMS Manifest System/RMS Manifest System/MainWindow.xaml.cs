﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Username_txtbx.Focus();
            //would like to make the database object public to rest, but this doesnt work, not necessary but would be nice
            //Admin_DB db = new Admin_DB();
        }

        private void Sign_In_Btn_Click(object sender, RoutedEventArgs e)
        {
            if (Username_txtbx.Text == null || Username_txtbx.Text == "" || Password_txtbx.Password == null || Password_txtbx.Password == "")
            {
                MessageBox.Show("Required information is missing please make sure all information is present.", "Missing Information", MessageBoxButton.OK);
            }
            else
            {
                string u_name = RMS_Security.SHA512(Username_txtbx.Text);
                string p_word = RMS_Security.SHA512(Password_txtbx.Password);

                //Database db = new Database();
                Admin_DB db = new Admin_DB();
                List<string> ls = db.retrieveAdminLoginInfo(u_name, p_word);
                /*foreach(var i in ls)
                {
                    Console.WriteLine(i);
                }*/

                /*Console.WriteLine(ls[0]);
                Console.WriteLine("Username: " + u_name);
                Console.WriteLine(ls[1]);
                Console.WriteLine("Password: " + p_word);
                Console.Read();*/
                if (ls.Count == 0)
                {
                    MessageBox.Show("The username and or password did not match. Please try again.", "Database Lookup Error", MessageBoxButton.OK);
                }
                else if (ls[0] != u_name)
                {
                    MessageBox.Show("The username entered does not match or may not exist. Please try again or go to the sign-up page.", "Username error", MessageBoxButton.OK);
                }
                else if (ls[1] != p_word)
                {
                    MessageBox.Show("The password entered is not correct. Please try again.", "Password error", MessageBoxButton.OK);
                }
                else
                {
                    //this may need to change in the future, but for now the user will be sent to the data_view_window

                    // Data_View_Window dv = new Data_View_Window();
                    //dv.Show();
                    //Jumper_Window jp = new Jumper_Window();
                    //jp.Show();
                    Home_Window home = new Home_Window();
                    home.Show();
                    this.Close();
                }
            }
        }
        private void Sign_Up_Btn_Click(object sender, RoutedEventArgs e)
        {
            RMS_Sign_Up_Window sign_up_win = new RMS_Sign_Up_Window();
            sign_up_win.Show();
            this.Close();
        }

        private void Connect_btn_Click(object sender, RoutedEventArgs e)
        {
            //Data_View_Window dv = new Data_View_Window();
            //dv.Show();
            //this.Close();
            //Jumper_Window jp = new Jumper_Window();
            //jp.Show();
            Home_Window home = new Home_Window();
            home.Show();
            this.Close();
        }
    }
}
