﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Manifest_Window.xaml
    /// </summary>
    public partial class Manifest_Window : Window
    {
        public Manifest_Window()
        {
            InitializeComponent();
            this.Closed += Manifest_Window_Closed;
            Loaded += Manifest_Window_Loaded;
        }

        private void Manifest_Window_Loaded(object sender, RoutedEventArgs e)
        {
            Plane_DB plane = new Plane_DB();
            planeInfo = plane.Plane_ID_Capacity();

            if(planeInfo.Count == 0)
            {
                SelectPlaneCmbBox.Items.Add("Missing Data : 0");
            }
            else
            {
                for(int i = 0; i < planeInfo.Count; i = i + 2)
                {
                    object ID = planeInfo[i];
                    object Cap = planeInfo[i + 1];
                    string item = ID + " : " + Cap;
                    SelectPlaneCmbBox.Items.Add(item);
                }
            }

            LoadNumberTextBox.IsEnabled = false;
            DataGridJumpersOnLoad.IsEnabled = false;
            LoadDate_DtPk.IsEnabled = false;
            JumpDate_DtPk.IsEnabled = false;
        }

        private void Manifest_Window_Closed(object sender, EventArgs e)
        {
            Home_Window home = new Home_Window();
            home.Show();
            this.Close();
        }

        private void ManifestLoadBtn_Click(object sender, RoutedEventArgs e)
        {
            List<string> jumperInfo = new List<string>();

            DataTable dt = new DataTable();
            dt = ((DataView)DataGridJumpersOnLoad.ItemsSource).ToTable();

            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    jumperInfo.Add(item.ToString());
                }
            }

            for (int i = 0; i < jumperInfo.Count; i = i + 3)
            {
                Jump_RMS jump = new Jump_RMS();

                try
                {
                    jump.Jump_Date = JumpDate_DtPk.SelectedDate.Value.Date;
                }
                catch(Exception excp)
                {
                    DateTime date = new DateTime(1776, 1, 1);
                    jump.Jump_Date = date.Date;
                }

                jump.Style = jumperInfo[i + 1];
                jump.Jumper_ID = jumperInfo[i];
                jump.Load_NO = LoadNumberTextBox.Text;

                try
                {
                    jump.Load_Date = LoadDate_DtPk.SelectedDate.Value.Date;
                }
                catch (Exception excp)
                {
                    DateTime date = new DateTime(1776, 1, 1);
                    jump.Load_Date = date.Date;

                }

                List<string> plane_id = new List<string>(SelectPlaneCmbBox.SelectedItem.ToString().Split(':'));
                jump.Plane_ID = plane_id[0].Trim();
                jump.Cost_Code = jumperInfo[i + 2];

                string errorCheckingOutput = jump.ErrorChecking_Add();

                if (!errorCheckingOutput.Equals(""))
                {
                    MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                    return;
                }

                string foreignKeyCheck = jump.ForeignKey_Check(jump.Jumper_ID, jump.Load_NO, jump.Load_Date, jump.Plane_ID, jump.Cost_Code);


                if (!foreignKeyCheck.Equals(""))
                {
                    MessageBox.Show(foreignKeyCheck, "Missing Information", MessageBoxButton.OK);
                    return;
                }

                else
                {
                    if (jump.Jumper_ID == null || jump.Jumper_ID == "")
                    {
                        jump.Jumper_ID = null;
                    }

                    if (jump.Load_NO == null || jump.Load_NO == "")
                    {
                        jump.Load_NO = null;
                    }

                    if (jump.Plane_ID == null || jump.Plane_ID == "")
                    {
                        jump.Plane_ID = null;
                    }

                    if (jump.Cost_Code == null || jump.Cost_Code == "")
                    {
                        jump.Cost_Code = null;
                    }

                    Jump_DB add_DB = new Jump_DB();
                    add_DB.Insert_New_Jumps(jump);
                }
            }

            JumpDate_DtPk.SelectedDate = new DateTime(1776, 1, 1);
            LoadNumberTextBox.Clear();
            LoadDate_DtPk.SelectedDate = new DateTime(1776, 1, 1);
            //need to test if this is the best way to remove all of the data from the rows or not
            DataGridJumpersOnLoad.ClearDetailsVisibilityForItem(DataGridJumpersOnLoad);

        }

        //Dictionary<object, object> planeInfo = new Dictionary<object, object>();
        List<string> planeInfo = new List<string>();

        private void SelectPlaneCmbBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectPlaneCmbBox.SelectedValue.ToString() == "Missing Data : 0")
            {
                MessageBox.Show("A connection to the database could not be established.","Connection Failed",MessageBoxButton.OK);
                return;
            }
            List <string> Items = new List<string> (SelectPlaneCmbBox.SelectedItem.ToString().Split(':'));
            int capacity = Convert.ToInt32(Items[1].Trim());
            LoadNumberTextBox.IsEnabled = true;
            DataGridJumpersOnLoad.IsEnabled = true;
            LoadDate_DtPk.IsEnabled = true;
            JumpDate_DtPk.IsEnabled = true;

            DataTable table = new DataTable();
            table.Columns.Add("JumperID", typeof(string));
            table.Columns.Add("JumperStyle", typeof(string));
            table.Columns.Add("CostCode", typeof(string));

            if (capacity != 0)
            {
                //i = 1 because when adding columns a row is added by default, so to account for that start the loop at 1
                //if we can stop the automatic row adding change the 1 to a 0
                for(int i = 1; i < capacity; i++)
                {
                    table.Rows.Add("", "");
                }
                
            }
            DataGridJumpersOnLoad.ItemsSource = table.DefaultView;

        }
    }
}
