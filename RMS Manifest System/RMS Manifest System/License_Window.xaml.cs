﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for License_Window.xaml
    /// </summary>
    public partial class License_Window : Window
    {
        public License_Window()
        {
            InitializeComponent();
            this.Closed += License_Window_Closed;
        }

        private void License_Window_Closed(object sender, EventArgs e)
        {
            Home_Window home = new Home_Window();
            home.Show();
            this.Close();
        }

        private void btnAddLicense_Click(object sender, RoutedEventArgs e)
        {
            License_RMS license = new License_RMS();
            license.USPA_MEM_NO = USPAMemNoTxtBxAddLicense.Text;
            license.USPA_LIC_NO = UPSALicNoTxtBxAddLicense.Text;
            try
            {
                license.USPA_EXP_Date = USPAExpDateAddLicense.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                license.USPA_EXP_Date = dt.Date;

            }

            string errorCheckingOutput = license.ErrorChecking();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            string uspa_ID_Check = license.USPA_MEM_NO_CHECK(USPAMemNoTxtBxAddLicense.Text);

            if (!uspa_ID_Check.Equals(null) || !uspa_ID_Check.Equals(""))
            {
                MessageBox.Show(uspa_ID_Check, "USPA Memeber Number Already in Use", MessageBoxButton.OK);
                return;
            }

            License_DB lic = new License_DB();
            lic.Insert_New_License(license);

            USPAMemNoTxtBxAddLicense.Clear();
            USPAMemNoTxtBxAddLicense.Clear();
            USPAExpDateAddLicense.SelectedDate = new DateTime(1776, 1, 1);
        }

        private void DeleteLicenseBtn_Click(object sender, RoutedEventArgs e)
        {
            License_RMS lic = new License_RMS();
            lic.USPA_MEM_NO = USPAMemNoTxtBxDeleteLicense.Text;

            if (lic.USPA_MEM_NO == null || lic.USPA_MEM_NO == "")
            {
                MessageBox.Show("A USPA Member Number must be entered in order for this to function correctly.", "Missing Jumper ID", MessageBoxButton.OK);
            }
            else
            {
                License_DB deleteUSPAMEMNO = new License_DB();
                deleteUSPAMEMNO.Delete_License(lic);

                USPAMemNoTxtBxDeleteLicense.Clear();
            }
        }

        private void SearchLicenseBtn_Click(object sender, RoutedEventArgs e)
        {
            License_RMS lic = new License_RMS();
            lic.USPA_MEM_NO = USPA_MEMBER_NO_txtbx_search.Text;

            if (lic.USPA_MEM_NO == null || lic.USPA_MEM_NO == "")
            {
                MessageBox.Show("A USPA Member Number must be entered in order for this to function correctly.", "Missing Jumper ID", MessageBoxButton.OK);
            }
            else
            {
                License_DB searchUSPAMEMNO = new License_DB();
                DataTable dt = searchUSPAMEMNO.Retrieve_LicenseForDisplay(lic);
                DatagridLicense.ItemsSource = dt.DefaultView;

                USPA_MEMBER_NO_txtbx_search.Clear();
            }
        }

        private void AllLicensesBtn_Click(object sender, RoutedEventArgs e)
        {
            License_DB allLicenses = new License_DB();
            DataTable dt = allLicenses.All_License();
            DatagridLicense.ItemsSource = dt.DefaultView;
        }

        private void Update_License_Btn_Click(object sender, RoutedEventArgs e)
        {
            License_RMS license = new License_RMS();
            license.USPA_MEM_NO = USPAMemNoTxtBxUpdateLicense.Content.ToString();
            license.USPA_LIC_NO = UPSALicNoTxtBxUpdateLicense.Text;
            try
            {
                license.USPA_EXP_Date = USPAExpDateUpdateLicense.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                license.USPA_EXP_Date = dt.Date;

            }

            string errorCheckingOutput = license.ErrorChecking();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            License_DB lic = new License_DB();
            lic.Update_License(license);

            USPAMemNoTxtBxUpdateLicense.Content = "";
            UPSALicNoTxtBxUpdateLicense.Clear();
            USPAExpDateUpdateLicense.SelectedDate = new DateTime(1776, 1, 1);
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

            switch (tabItem)
            {
                case "Update License":
                    Update_Window uw = new Update_Window();
                    uw.Table_ID_Lbl.Content = "USPA Member Numer:";
                    try
                    {
                        uw.ShowDialog();
                    }
                    catch (Exception excp)
                    {
                        return;
                    }


                    string USPA_MEM_NO_Update = "";
                    USPA_MEM_NO_Update = uw.ID_Return;
                    uw.Close();

                    if (USPA_MEM_NO_Update == null || USPA_MEM_NO_Update == "")
                    {
                        return;
                    }
                    else
                    {
                        License_RMS lic = new License_RMS();
                        lic.USPA_MEM_NO = USPA_MEM_NO_Update;
                        License_DB ret_DB = new License_DB();
                        List<string> retrievedCostUpdate = ret_DB.Retrieve_LicenseForUpdate(lic);
                        try
                        {
                            USPAMemNoTxtBxUpdateLicense.Content = retrievedCostUpdate[0];
                            UPSALicNoTxtBxUpdateLicense.Text = retrievedCostUpdate[1];
                            USPAExpDateUpdateLicense.Text = retrievedCostUpdate[2];
                        }
                        catch(Exception excp)
                        {
                            MessageBox.Show("The requested data could not be retrieved.", "Error", MessageBoxButton.OK);
                        }

                    }

                    return;

                default:
                    return;
            }
        }

        private void Link_Rating_Btn_Click(object sender, RoutedEventArgs e)
        {
            Has_Rating_RMS hs = new Has_Rating_RMS();
            hs.USPA_MEM_NO = USPA_MEM_NO_Link_TxtBx.Text;
            hs.Rating_ID = Rating_ID_Link_TxtBx.Text;

            string errorCheckingOutput = hs.ErrorChecking();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            string foreignKeyCheck = hs.ForeignKey_Check(hs.USPA_MEM_NO, hs.Rating_ID);

            if (!foreignKeyCheck.Equals(""))
            {
                MessageBox.Show(foreignKeyCheck, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                Has_Rating_DB addHS = new Has_Rating_DB();
                addHS.Insert_New_Has_Rating(hs);

                USPA_MEM_NO_Link_TxtBx.Clear();
                Rating_ID_Link_TxtBx.Clear();
            }
        }

        private void Unlink_Rating_Btn_Click(object sender, RoutedEventArgs e)
        {
            Has_Rating_RMS hs = new Has_Rating_RMS();
            hs.USPA_MEM_NO = USPA_MEM_NO_Unlink_TxtBx.Text;
            hs.Rating_ID = Rating_ID_Unlink_TxtBx.Text;

            string errorCheckingOutput = hs.ErrorChecking();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            string foreignKeyCheck = hs.ForeignKey_Check(hs.USPA_MEM_NO, hs.Rating_ID);

            if (!foreignKeyCheck.Equals(""))
            {
                MessageBox.Show(foreignKeyCheck, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                Has_Rating_DB deleteHS = new Has_Rating_DB();
                deleteHS.Delete_Has_Rating(hs);

                USPA_MEM_NO_Unlink_TxtBx.Clear();
                Rating_ID_Unlink_TxtBx.Clear();
            }
        }

        private void SearchAll_Rating_Links_Btn_Click(object sender, RoutedEventArgs e)
        {
            Has_Rating_DB all_Has_Ratings = new Has_Rating_DB();
            DataTable dt = all_Has_Ratings.All_Has_Ratings();
            RatingLinkDataGrid.ItemsSource = dt.DefaultView;
        }

        private void USPA_MEM_SearchAll_Links_Btn_Click(object sender, RoutedEventArgs e)
        {
            Has_Rating_RMS hs = new Has_Rating_RMS();
            hs.USPA_MEM_NO = USPA_MEM_NO_SearchLink_TxtBx.Text;

            if (hs.USPA_MEM_NO == null || hs.USPA_MEM_NO == "")
            {
                MessageBox.Show("A USPA Member Number must be entered in order for this to function correctly.", "Missing USPA Member Number", MessageBoxButton.OK);
            }
            else
            {
                Has_Rating_DB searchHSDB = new Has_Rating_DB();
                DataTable dt = searchHSDB.Retrieve_USPA_MEM_Has_Rating_ForDisplay(hs);
                RatingLinkDataGrid.ItemsSource = dt.DefaultView;

                USPA_MEM_NO_SearchLink_TxtBx.Clear();
            }
        }
    }
}
