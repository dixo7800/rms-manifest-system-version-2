﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Load_Window.xaml
    /// </summary>
    public partial class Load_Window : Window
    {
        public Load_Window()
        {
            InitializeComponent();
            this.Closed += Load_Window_Closed;
        }

        private void Load_Window_Closed(object sender, EventArgs e)
        {
            Home_Window home = new Home_Window();
            home.Show();
            this.Close();
        }

        private void AddLoad_Btn_Click(object sender, RoutedEventArgs e)
        {
            Load_RMS load = new Load_RMS();

            try
            {
                load.Load_Date = LoadDate_Add_DtPk.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                load.Load_Date = dt.Date;

            }

            load.Pilot = Pilot_Add_TxtBx.Text;
            load.Fuel_Load = FuelLoad_Add_TxtBx.Text;

            string errorCheckingOutput = load.ErrorChecking_Add();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                Load_DB add_DB = new Load_DB();
                add_DB.Insert_New_Load(load);

                LoadDate_Add_DtPk.SelectedDate = new DateTime(1776, 1, 1);
                Pilot_Add_TxtBx.Clear();
                FuelLoad_Add_TxtBx.Clear();
            }
        }

        private void UpdateLoad_Btn_Click(object sender, RoutedEventArgs e)
        {
            Load_RMS load = new Load_RMS();
            load.Load_NO = LoadNo_Update_Lbl.Content.ToString();

            try
            {
                load.Load_Date = LoadDate_Update_DtPk.SelectedDate.Value.Date;
            }
            catch (Exception excp)
            {
                DateTime dt = new DateTime(1776, 1, 1);
                load.Load_Date = dt.Date;

            }

            load.Pilot = Pilot_Update_TxtBx.Text;
            load.Fuel_Load = FuelLoad_Update_TxtBx.Text;

            string errorCheckingOutput = load.ErrorChecking_Update();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                Load_DB update_DB = new Load_DB();
                update_DB.Update_Load(load);

                LoadNo_Update_Lbl.Content = "";
                LoadDate_Update_DtPk.SelectedDate = new DateTime(1776, 1, 1);
                Pilot_Update_TxtBx.Clear();
                FuelLoad_Update_TxtBx.Clear();
            }
        }

        private void DeleteLoad_Btn_Click(object sender, RoutedEventArgs e)
        {
            Load_RMS load = new Load_RMS();
            load.Load_NO = LoadNo_Delete_TxtBx.Text;
            load.Load_Date = LoadDate_Delete_DtPk.SelectedDate.Value.Date;

            if (load.Load_NO == null || load.Load_NO == "")
            {
                MessageBox.Show("A Load Number must be entered in order for this to function correctly.", "Missing Load Number", MessageBoxButton.OK);
            }
            else
            {
                Load_DB deleteLoadDB = new Load_DB();
                deleteLoadDB.Delete_Load(load);

                LoadNo_Delete_TxtBx.Clear();
                LoadDate_Delete_DtPk.SelectedDate = new DateTime(1776, 1, 1);
            }
        }

        private void SearchLoad_Btn_Click(object sender, RoutedEventArgs e)
        {
            Load_RMS load = new Load_RMS();
            load.Load_NO = LoadNo_Search_TxtBx.Text;
            load.Load_Date = LoadDate_Search_DtPk.SelectedDate.Value.Date;

            if (load.Load_NO == null || load.Load_NO == "")
            {
                MessageBox.Show("A Load Number must be entered in order for this to function correctly.", "Missing Load Number", MessageBoxButton.OK);
            }
            else
            {
                Load_DB searchLoadDB = new Load_DB();
                DataTable dt = searchLoadDB.Retrieve_LoadForDisplay(load);
                dataGridLoad.ItemsSource = dt.DefaultView;

                LoadNo_Search_TxtBx.Clear();
                LoadDate_Search_DtPk.SelectedDate = new DateTime(1776, 1, 1);
            }
        }

        private void SearchAllLoad_Btn_Click(object sender, RoutedEventArgs e)
        {
            Load_DB allLoads = new Load_DB();
            DataTable dt = allLoads.All_Loads();
            dataGridLoad.ItemsSource = dt.DefaultView;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

            switch (tabItem)
            {
                //this isnt right! need to accept an id and a date!!!!!!!!!!!!!!!!!!!!!!!
                case "Update Load":
                    Update_Window_TwoInputs uw = new Update_Window_TwoInputs();
                    uw.Table_ID_Lbl.Content = "Load Number:";
                    uw.Table_Date_Lbl.Content = "Load Date:";

                    try
                    {
                        uw.ShowDialog();
                    }
                    catch (Exception excp)
                    {
                        return;
                    }

                    string Load_Number_Update = uw.ID_Return;
                    DateTime Load_Date_Update = uw.Date_Return;
                    
                    uw.Close();

                    if (Load_Number_Update == null || Load_Number_Update == "")
                    {
                        return;
                    }
                    else if (Load_Date_Update == null || Load_Date_Update.Date.ToString().Contains("1/1/1776"))
                    {
                        return;
                    }
                    else
                    {
                        Load_RMS load = new Load_RMS();
                        load.Load_NO = Load_Number_Update;
                        load.Load_Date = Load_Date_Update.Date;
                        Load_DB ret_DB = new Load_DB();
                        List<string> retrievedLoadUpdate = ret_DB.Retrieve_LoadForUpdate(load);

                        try
                        {
                            LoadNo_Update_Lbl.Content = retrievedLoadUpdate[0];
                            LoadDate_Update_DtPk.Text = retrievedLoadUpdate[1];
                            Pilot_Update_TxtBx.Text = retrievedLoadUpdate[2];
                            FuelLoad_Update_TxtBx.Text = retrievedLoadUpdate[3];
                        }
                        catch (Exception excp)
                        {
                            MessageBox.Show("The requested data could not be retrieved.", "Error", MessageBoxButton.OK);
                        }

                    }

                    return;

                default:
                    return;
            }
        }
    }
}
