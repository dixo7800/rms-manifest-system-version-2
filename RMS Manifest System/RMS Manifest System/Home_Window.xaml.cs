﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Home_Window.xaml
    /// </summary>
    public partial class Home_Window : Window
    {
        public Home_Window()
        {
            InitializeComponent();
            //this.Closed += Home_Window_Closed;
        }

        private void Home_Window_Closed(object sender, EventArgs e)
        {
            MainWindow mw = new MainWindow();
            mw.Show();
            this.Close();
        }

        private void Open_Jumper_Btn_Click(object sender, RoutedEventArgs e)
        {
            Jumper_Window jp = new Jumper_Window();
            jp.Show();
            this.Close();
            
        }

        private void Open_License_Btn_Click(object sender, RoutedEventArgs e)
        {
            License_Window lc = new License_Window();
            lc.Show();
            this.Close();
        }

        private void Open_Rig_Btn_Click(object sender, RoutedEventArgs e)
        {
            Rig_Window rg = new Rig_Window();
            rg.Show();
            this.Close();
        }

        private void Open_Plane_Btn_Click(object sender, RoutedEventArgs e)
        {
            Plane_Window plane = new Plane_Window();
            plane.Show();
            this.Close();
        }

        private void Open_Manifest_Btn_Click(object sender, RoutedEventArgs e)
        {
            Manifest_Window manifest = new Manifest_Window();
            manifest.Show();
            this.Close();
        }

        private void Open_Jump_Btn_Click(object sender, RoutedEventArgs e)
        {
            Jump_Window jump = new Jump_Window();
            jump.Show();
            this.Close();
        }

        private void Open_Cost_Btn_Click(object sender, RoutedEventArgs e)
        {
            Cost_Window cost = new Cost_Window();
            cost.Show();
            this.Close();
        }

        private void Open_Rating_Btn_Click(object sender, RoutedEventArgs e)
        {
            Rating_Window rating = new Rating_Window();
            rating.Show();
            this.Close();
        }

        private void Open_Loads_Btn_Click(object sender, RoutedEventArgs e)
        {
            Load_Window load = new Load_Window();
            load.Show();
            this.Close();
        }

        private void Open_Reports_Btn_Click(object sender, RoutedEventArgs e)
        {
            Report_Window report = new Report_Window();
            report.Show();
            this.Close();
            //MessageBox.Show("This feature is not implemented yet, come back after the next update.", "Coming Soon", MessageBoxButton.OK);
        }
    }
}
