﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Update_Window.xaml
    /// </summary>
    public partial class Update_Window : Window
    {
        public Update_Window()
        {
            InitializeComponent();
        }

        private void Submit_Btn_Click(object sender, RoutedEventArgs e)
        {
            ID_Return = Table_ID_TxtBx.Text;
            this.Hide();
        }

        private void Cancel_Btn_Click(object sender, RoutedEventArgs e)
        {
            Table_ID_TxtBx.Text = "";
            ID_Return = Table_ID_TxtBx.Text;
            this.Hide();
        }

        public string ID_Return = "";
    }
}
