﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Update_Jumper_ID_DialogWindow.xaml
    /// </summary>
    public partial class Update_Jumper_ID_DialogWindow : Window
    {
        public Update_Jumper_ID_DialogWindow()
        {
            InitializeComponent();
        }

        private void submitBtn_Click(object sender, RoutedEventArgs e)
        {
            jumper_ID = jumper_id_txtbx.Text;
            this.Hide();
        }

        private void Cancelbtn_Click(object sender, RoutedEventArgs e)
        {
            jumper_id_txtbx.Text = "";
            jumper_ID = jumper_id_txtbx.Text;
            this.Hide();
        }

        public string jumper_ID = "";
    }
}
