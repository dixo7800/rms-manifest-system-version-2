﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Rating_RMS
    {
        public string Rating_ID { get; set; }
        public string Rating { get; set; }

        public string ErrorChecking_Add()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("Rating", Rating);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "" || d.Value.ToString().Contains("1/1/1776"))
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }

        public string ErrorChecking_Update()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("Rating ID", Rating_ID);
            dict.Add("Rating", Rating);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "" || d.Value.ToString().Contains("1/1/1776"))
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }
    }
}
