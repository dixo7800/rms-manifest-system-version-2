﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Report_Window.xaml
    /// </summary>
    public partial class Report_Window : Window
    {
        public Report_Window()
        {
            InitializeComponent();
            Loaded += Report_Window_Loaded;
        }

        private void Report_Window_Loaded(object sender, RoutedEventArgs e)
        {
            ID_TxtBx.IsEnabled = false;
            Date_DtPk.IsEnabled = false;
            Output_DataGrid.IsEnabled = false;
            Search_Btn.IsEnabled = false;

            Operation_CmbBx.Items.Add("All Jumps");
            Operation_CmbBx.Items.Add("All Jumps By Jumper");
            Operation_CmbBx.Items.Add("All Jumps Today");
            Operation_CmbBx.Items.Add("All Jumps Specific Date");
            Operation_CmbBx.Items.Add("All Jumps Specific Jumper and Date");

        }

        private void Operation_CmbBx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Operation_CmbBx.SelectedValue.ToString() == "All Jumps")
            {
                ID_TxtBx.IsEnabled = false;
                Date_DtPk.IsEnabled = false;
                Search_Btn.IsEnabled = true;
            }
            else if (Operation_CmbBx.SelectedValue.ToString() == "All Jumps By Jumper")
            {
                Date_DtPk.IsEnabled = false;
                ID_TxtBx.IsEnabled = true;
                Search_Btn.IsEnabled = true;
            }
            else if (Operation_CmbBx.SelectedValue.ToString() == "All Jumps Today")
            {
                ID_TxtBx.IsEnabled = false;
                Date_DtPk.IsEnabled = false;
                Search_Btn.IsEnabled = true;
            }
            else if (Operation_CmbBx.SelectedValue.ToString() == "All Jumps Specific Date")
            {
                ID_TxtBx.IsEnabled = false;
                Date_DtPk.IsEnabled = true;
                Search_Btn.IsEnabled = true;
            }
            else if (Operation_CmbBx.SelectedValue.ToString() == "All Jumps Specific Jumper and Date")
            {
                ID_TxtBx.IsEnabled = true;
                Date_DtPk.IsEnabled = true;
                Search_Btn.IsEnabled = true;
            }
            else
            {
                return;
            }
        }

        private void Search_Btn_Click(object sender, RoutedEventArgs e)
        {
            if(Operation_CmbBx.SelectedValue.ToString() == "All Jumps")
            {
                Output_DataGrid.IsEnabled = true;

                Report_DB report = new Report_DB();
                DataTable dt = report.Report_All_Jumps();
                Output_DataGrid.ItemsSource = dt.DefaultView;
                
            }
            else if (Operation_CmbBx.SelectedValue.ToString() == "All Jumps By Jumper")
            {
                Output_DataGrid.IsEnabled = true;

                string Jumper_ID = ID_TxtBx.Text;

                if(Jumper_ID == null || Jumper_ID == "")
                {
                    MessageBox.Show("The ID textbox did not have any data. The operation could not be completed.", "Missing Data", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    Report_DB report = new Report_DB();
                    DataTable dt = report.Report_All_Jumps_By_Jumper(Jumper_ID);
                    Output_DataGrid.ItemsSource = dt.DefaultView;

                    ID_TxtBx.Clear();
                }

            }
            else if (Operation_CmbBx.SelectedValue.ToString() == "All Jumps Today")
            {
                Output_DataGrid.IsEnabled = true;

                DateTime date = DateTime.Today.Date;

                Report_DB report = new Report_DB();
                DataTable dt = report.Report_All_Jumps_Today(date);
                Output_DataGrid.ItemsSource = dt.DefaultView;

                Date_DtPk.SelectedDate = new DateTime(1776, 1, 1);
            }
            else if (Operation_CmbBx.SelectedValue.ToString() == "All Jumps Specific Date")
            {
                Output_DataGrid.IsEnabled = true;

                DateTime date = new DateTime(1776, 1, 1);

                try
                {
                    date = Date_DtPk.SelectedDate.Value.Date;
                }
                catch(Exception excp)
                {
                    
                }
                

                if(date.Date.ToString().Contains("1/1/1776"))
                {
                    MessageBox.Show("The Date information was left blank. The operation could not be completed.", "Missing Data", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    Report_DB report = new Report_DB();
                    DataTable dt = report.Report_All_Jumps_Specific_Date(date);
                    Output_DataGrid.ItemsSource = dt.DefaultView;

                    Date_DtPk.SelectedDate = new DateTime(1776, 1, 1);
                }
     
            }
            else if (Operation_CmbBx.SelectedValue.ToString() == "All Jumps Specific Jumper and Date")
            {
                Output_DataGrid.IsEnabled = true;

                string Jumper_ID = ID_TxtBx.Text;
                DateTime date = new DateTime(1776, 1, 1);

                try
                {
                    date = Date_DtPk.SelectedDate.Value.Date;
                }
                catch(Exception excp)
                {

                }
                
                if (Jumper_ID == null || Jumper_ID == "")
                {
                    MessageBox.Show("The ID textbox did not have any data. The operation could not be completed.", "Missing Data", MessageBoxButton.OK);
                    return;
                }
                else if (date.Date.ToString().Contains("1/1/1776"))
                {
                    MessageBox.Show("The Date information was left blank. The operation could not be completed.", "Missing Data", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    Report_DB report = new Report_DB();
                    DataTable dt = report.Report_All_Jumps_Specific_Jumper_Date(Jumper_ID, date);
                    Output_DataGrid.ItemsSource = dt.DefaultView;

                    ID_TxtBx.Clear();
                    Date_DtPk.SelectedDate = new DateTime(1776, 1, 1);
                }

            }
            else
            {
                return;
            }
        }
    }
}
