﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Rating_Window.xaml
    /// </summary>
    public partial class Rating_Window : Window
    {
        public Rating_Window()
        {
            InitializeComponent();
            this.Closed += Rating_Window_Closed;
        }

        private void Rating_Window_Closed(object sender, EventArgs e)
        {
            Home_Window home = new Home_Window();
            home.Show();
            this.Close();
        }

        private void btnAddRating_Click(object sender, RoutedEventArgs e)
        {
            Rating_RMS rating = new Rating_RMS();
            //rating.Rating_ID = RatingID_txtbx.Text;
            rating.Rating = Rating_add_txtbx.Text;

            string errorCheckingOutput = rating.ErrorChecking_Add();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                Rating_DB addDB = new Rating_DB();
                addDB.Insert_New_Ratings(rating);

                Rating_add_txtbx.Clear();
            }
        }

        private void btnUpdateRating_Click(object sender, RoutedEventArgs e)
        {
            Rating_RMS rating = new Rating_RMS();
            rating.Rating_ID = Rating_ID_Update_Lbl.Content.ToString();
            rating.Rating = Rating_Update_TxtBx.Text;

            string errorCheckingOutput = rating.ErrorChecking_Update();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                Rating_DB updateDB = new Rating_DB();
                updateDB.Update_Ratings(rating);

                Rating_ID_Update_Lbl.Content = "";
                Rating_Update_TxtBx.Clear();
            }
        }

        private void btnDeleteRating_Click(object sender, RoutedEventArgs e)
        {
            Rating_RMS rating = new Rating_RMS();
            rating.Rating_ID = Rating_ID_Delete_TxtBx.Text;

            if (rating.Rating_ID == null || rating.Rating_ID == "")
            {
                MessageBox.Show("A Rating ID must be entered in order for this to function correctly.", "Missing Rating ID", MessageBoxButton.OK);
            }
            else
            {
                Rating_DB deleteRatingDB = new Rating_DB();
                deleteRatingDB.Delete_Ratings(rating);

                Rating_ID_Delete_TxtBx.Clear();
            }
        }

        private void btnSearchRating_Click(object sender, RoutedEventArgs e)
        {
            Rating_RMS rating = new Rating_RMS();
            rating.Rating_ID = RatingID_txtbx_SearchRating.Text;

            if (rating.Rating_ID == null || rating.Rating_ID == "")
            {
                MessageBox.Show("A Rating ID must be entered in order for this to function correctly.", "Missing Rating ID", MessageBoxButton.OK);
            }
            else
            {
                Rating_DB searchRatingDB = new Rating_DB();
                DataTable dt = searchRatingDB.Retrieve_RatingsForDisplay(rating);
                DataGrid_Rating.ItemsSource = dt.DefaultView;

                RatingID_txtbx_SearchRating.Clear();
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

            switch (tabItem)
            {
                case "Update Ratings":
                    Update_Window uw = new Update_Window();
                    uw.Table_ID_Lbl.Content = "Rating ID:";

                    try
                    {
                        uw.ShowDialog();
                    }
                    catch (Exception excp)
                    {
                        return;
                    }

                    string Rating_ID_Update = "";
                    Rating_ID_Update = uw.ID_Return;
                    uw.Close();

                    if (Rating_ID_Update == null || Rating_ID_Update == "")
                    {
                        return;
                    }
                    else
                    {
                        Rating_RMS rating = new Rating_RMS();
                        rating.Rating_ID = Rating_ID_Update;
                        Rating_DB ret_DB = new Rating_DB();
                        List<string> retrievedRatingUpdate = ret_DB.Retrieve_RatingsForUpdate(rating);

                        try
                        {
                            Rating_ID_Update_Lbl.Content = retrievedRatingUpdate[0];
                            Rating_Update_TxtBx.Text = retrievedRatingUpdate[1];
                        }
                        catch(Exception excp)
                        {
                            MessageBox.Show("The requested data could not be retrieved.", "Error", MessageBoxButton.OK);
                        }

                    }

                    return;

                default:
                    return;
            }
        }
    }
}
