﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Has_Rating_RMS
    {
        public string USPA_MEM_NO;
        public string Rating_ID;

        public string ErrorChecking()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("USPA Member Number", USPA_MEM_NO);
            dict.Add("Rating ID", Rating_ID);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "")
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }

        private string ForeignKeyCheck_USPA_MEM_NO(string USPANUM)
        {
            string message = "";


            License_DB license = new License_DB();
            string value = license.USPA_MEM_NO_Lookup(USPANUM);

            if (!value.Equals(""))
            {
                message = "";
            }
            else
            {
                message = " The USPA Memeber Number that was referenced is not in the database. ";
            }


            return message;
        }

        private string ForeignKeyCheck_Rating_ID(string ratingID)
        {
            string message = "";


            Rating_DB rating = new Rating_DB();
            string value = rating.RATING_ID_Lookup(ratingID);

            if (!value.Equals(""))
            {
                message = "";
            }
            else
            {
                message = " The Rating ID that was referenced is not in the database. ";
            }


            return message;
        }

        public string ForeignKey_Check(string USPANUM, string ratingid)
        {
            string message = "";

            string a = ForeignKeyCheck_USPA_MEM_NO(USPANUM);

            string b = ForeignKeyCheck_Rating_ID(ratingid);

            message = message + a + b;

            return message;
        }
    }
}
