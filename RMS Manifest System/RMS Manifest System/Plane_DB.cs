﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Plane_DB:Database
    {
        public void Add_Plane(Plane_RMS plane)
        {
            string storedProcedureName = "Insert_New_Plane";
            List<string> storedProcedureVariables = new List<string>() { "@plane_id", "@plane_make", "@plane_model", "@fuel_type", "@jumper_capacity", "@annual_date", "@service_date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.Date };
            List<object> Values = new List<object>() { plane.Plane_ID, plane.Plane_Make, plane.Plane_Model, plane.Fuel_Type, plane.Jumper_Capacity, plane.Annual_Date, plane.Service_Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Update_Plane(Plane_RMS plane)
        {
            string storedProcedureName = "Update_Plane";
            List<string> storedProcedureVariables = new List<string>() { "@plane_id", "@plane_make", "@plane_model", "@fuel_type", "@jumper_capacity", "@annual_date", "@service_date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.Date };
            List<object> Values = new List<object>() { plane.Plane_ID, plane.Plane_Make, plane.Plane_Model, plane.Fuel_Type, plane.Jumper_Capacity, plane.Annual_Date, plane.Service_Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_Plane(Plane_RMS plane)
        {
            string storedProcedureName = "Delete_Plane";
            List<string> storedProcedureVariables = new List<string>() { "@plane_id" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { plane.Plane_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }


        public List<string> Retrieve_PlaneForUpdate(Plane_RMS plane)
        {
            string storedProcedureName = "Retrieve_Plane";
            string dataTableName = "Plane";
            List<string> storedProcedureVariables = new List<string>() { "@plane_id" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { plane.Plane_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> planeInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    planeInfo.Add(item.ToString());
                }

            }

            return planeInfo;
        }

        public DataTable Retrieve_PlaneForDisplay(Plane_RMS plane)
        {
            string storedProcedureName = "Retrieve_Plane";
            string dataTableName = "Plane";
            List<string> storedProcedureVariables = new List<string>() { "@plane_id" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { plane.Plane_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        //need to make stored procedure method for plane capacity dictionary return for manifest plane dropdown menu
        public List<string> Plane_ID_Capacity()
        {
            try
            {
                string storedProcedureName = "Plane_ID_Capacity";
                string dataTableName = "Plane";

                SqlCommand cmd = new SqlCommand();

                DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

                //Need to go through the datatable and pull the values into the dictionary where the plane id is the key and the capacity is the value!!!!!!!!!!!!!!!!!!

                //this is just a temporary solution - would rather dump directly into a dictionary but know how to make list<string>, so doing this for now
                List<string> planeInfo = new List<string>();
                foreach (DataRow row in dt.Rows)
                {
                    foreach (var item in row.ItemArray)
                    {
                        planeInfo.Add(item.ToString());
                    }

                }

                /*Dictionary<object, object> dict = new Dictionary<object, object>();

                for (int i = 0; i < planeInfo.Count(); i = i + 2)
                {
                    dict.Add(planeInfo[i], planeInfo[i + 1]);
                }*/

                //return dict;

                return planeInfo;
            }
            catch (Exception excp)
            {
                /*Dictionary<object, object> dict = new Dictionary<object, object>();

                return dict;*/
                List<string> planeInfo = new List<string>();
                return planeInfo;
            }

        }

        public string PLANE_ID_Lookup(string PLANE_ID)
        {
            string result = "";

            string storedProcedureName = "PLANE_ID_Lookup";
            string dataTableName = "PLANE_ID";
            List<string> storedProcedureVariables = new List<string>() { "@PLANE_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { PLANE_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            if (dt == null)
            {
                result = "";
                return result;
            }

            List<string> plane_pk = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    plane_pk.Add(item.ToString());
                }

            }

            if (plane_pk.Count == 0)
            {
                result = "";
                return result;
            }

            result = plane_pk[0];

            return result;
        }
    }
}
