﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Rig_DB:Database
    {
        //new allRigs function
        public DataTable allRigs()
        {
            string storedProcedureName = "ALL_RIGS";
            string dataTableName = "Rigs";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public void Insert_New_Rig(Rig_RMS rig)
        {
            object var1 = null;

            if (rig.Jumper_ID == null || rig.Jumper_ID == "")
            {
                var1 = DBNull.Value;
            }
            else
            {
                var1 = rig.Jumper_ID;
            }

            string storedProcedureName = "Insert_New_Rig";
            List<string> storedProcedureVariables = new List<string>() { "@CONTAINER_TYPE", "@CONTAINER_COLORS", "@MAIN_CANOPY", "@MAIN_COLORS", "@RESERVE_CANOPY", "@RESERVE_REPACK_DATE", "@AAD_TYPE", "@TANDEM", "@RENTAL", "@JUMPER_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Char, SqlDbType.VarChar };
            List<object> Values = new List<object>() { rig.Container_Type, rig.Container_Colors, rig.Main_Canopy, rig.Reserve_Canopy, rig.Reserve_Repack_Date, rig.AAD_Type, rig.Tandem, rig.Rental, var1 };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Update_Rig(Rig_RMS rig)
        {
            object var1 = null;

            if (rig.Jumper_ID == null || rig.Jumper_ID == "")
            {
                var1 = DBNull.Value;
            }
            else
            {
                var1 = rig.Jumper_ID;
            }

            string storedProcedureName = "Update_Rig";
            List<string> storedProcedureVariables = new List<string>() { "@RIG_ID", "@CONTAINER_TYPE", "@CONTAINER_COLORS", "@MAIN_CANOPY", "@MAIN_COLORS", "@RESERVE_CANOPY", "@RESERVE_REPACK_DATE", "@AAD_TYPE", "@TANDEM", "@RENTAL", "@JUMPER_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Char, SqlDbType.VarChar };
            List<object> Values = new List<object>() { rig.Rig_ID, rig.Container_Type, rig.Container_Colors, rig.Main_Canopy, rig.Reserve_Canopy, rig.Reserve_Repack_Date, rig.AAD_Type, rig.Tandem, rig.Rental, var1 };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_Rig(Rig_RMS rig)
        {
            string storedProcedureName = "Delete_Rig";
            List<string> storedProcedureVariables = new List<string>() { "@RIG_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { rig.Rig_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public List<string> Retrieve_RigForUpdate(Rig_RMS rig)
        {
            string storedProcedureName = "Retrieve_Rig";
            string dataTableName = "Rig";
            List<string> storedProcedureVariables = new List<string>() { "@Rig_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { rig.Rig_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> rigInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    rigInfo.Add(item.ToString());
                }

            }

            return rigInfo;
        }

        public DataTable Retrieve_RigForDisplay(Rig_RMS rig)
        {
            string storedProcedureName = "Retrieve_Rig";
            string dataTableName = "Rig";
            List<string> storedProcedureVariables = new List<string>() { "@Rig_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { rig.Rig_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }
    }
}
