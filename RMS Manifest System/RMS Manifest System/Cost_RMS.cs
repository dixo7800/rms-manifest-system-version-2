﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Cost_RMS
    {
        public string Cost_Code { get; set; }
        public string Cost_Name { get; set; }
        public string Cost_Price { get; set; }

        public string ErrorChecking_Add()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("Cost Name", Cost_Name);
            dict.Add("Cost Price", Cost_Price);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "")
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;  
        }

        public string ErrorChecking_Update()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("Cost Code", Cost_Code);
            dict.Add("Cost Name", Cost_Name);
            dict.Add("Cost Price", Cost_Price);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "")
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }
    }
}
