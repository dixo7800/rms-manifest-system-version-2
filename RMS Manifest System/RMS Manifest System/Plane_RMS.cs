﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Plane_RMS
    {
        public string Plane_ID { get; set; }
        public string Plane_Make { get; set; }
        public string Plane_Model { get; set; }
        public string Fuel_Type { get; set; }
        public string Jumper_Capacity { get; set; }
        public DateTime Annual_Date { get; set; }
        public DateTime Service_Date { get; set; }

        public string ErrorChecking_Add()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("Plane Make", Plane_Make);
            dict.Add("Plane Model", Plane_Model);
            dict.Add("Fuel Type", Fuel_Type);
            dict.Add("Jumper Capacity", Jumper_Capacity);
            dict.Add("Annual Date", Annual_Date);
            dict.Add("Service Date", Service_Date);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "" || d.Value.ToString().Contains("1/1/1776"))
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }

        public string ErrorChecking_Update()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("Plane ID", Plane_ID);
            dict.Add("Plane Make", Plane_Make);
            dict.Add("Plane Model", Plane_Model);
            dict.Add("Fuel Type", Fuel_Type);
            dict.Add("Jumper Capacity", Jumper_Capacity);
            dict.Add("Annual Date", Annual_Date);
            dict.Add("Service Date", Service_Date);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "" || d.Value.ToString().Contains("1/1/1776"))
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }
    }
}
