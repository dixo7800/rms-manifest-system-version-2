﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Report_DB:Database
    {
        public DataTable Report_All_Jumps()
        {
            string storedProcedureName = "Report_All_Jumps";
            string dataTableName = "All Jumps";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public DataTable Report_All_Jumps_By_Jumper(string Jumper_ID)
        {
            string storedProcedureName = "Report_All_Jumps_By_Jumper";
            string dataTableName = "All Jumps";

            List<string> storedProcedureVariables = new List<string>() { "@Jumper_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Jumper_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public DataTable Report_All_Jumps_Today(DateTime Today_Date)
        {
            string storedProcedureName = "Report_All_Jumps_Today";
            string dataTableName = "All Jumps";

            List<string> storedProcedureVariables = new List<string>() { "@Today_Date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.Date };
            List<object> Values = new List<object>() { Today_Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public DataTable Report_All_Jumps_Specific_Date(DateTime Date)
        {
            string storedProcedureName = "Report_All_Jumps_Specific_Date";
            string dataTableName = "All Jumps";

            List<string> storedProcedureVariables = new List<string>() { "@Date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.Date };
            List<object> Values = new List<object>() { Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public DataTable Report_All_Jumps_Specific_Jumper_Date(string Jumper_ID, DateTime Date)
        {
            string storedProcedureName = "Report_All_Jumps_Specific_Jumper_Date";
            string dataTableName = "All Jumps";

            List<string> storedProcedureVariables = new List<string>() { "@Jumper_ID", "@Date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date };
            List<object> Values = new List<object>() { Jumper_ID, Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }
    }
}
