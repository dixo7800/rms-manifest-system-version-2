﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Cost_Window.xaml
    /// </summary>
    public partial class Cost_Window : Window
    {
        public Cost_Window()
        {
            InitializeComponent();
            this.Closed += Cost_Window_Closed;
        }

        private void Cost_Window_Closed(object sender, EventArgs e)
        {
            Home_Window home = new Home_Window();
            home.Show();
            this.Close();
        }

        private void btnAddCostCode_Click(object sender, RoutedEventArgs e)
        {
            Cost_RMS cost = new Cost_RMS();
            cost.Cost_Name = txtCostName.Text;
            cost.Cost_Price = txtCostPrice.Text;

            string errorCheckingOutput = cost.ErrorChecking_Add();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                Cost_DB addCost = new Cost_DB();
                addCost.Insert_New_Cost(cost);

                txtCostName.Clear();
                txtCostPrice.Clear();
            }
        }

        private void btnUpdateCostCode_Click(object sender, RoutedEventArgs e)
        {
            Cost_RMS cost = new Cost_RMS();
            cost.Cost_Code = cost_code_lbl_update.Content.ToString();
            cost.Cost_Name = txtCostName_UpdateCostCode.Text;
            cost.Cost_Price = txtCostPrice_UpdateCostCode.Text;

            string errorCheckingOutput = cost.ErrorChecking_Update();

            if (!errorCheckingOutput.Equals(""))
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
                return;
            }

            else
            {
                Cost_DB updateCost = new Cost_DB();
                updateCost.Update_Cost(cost);
                cost_code_lbl_update.Content = "";
                txtCostName_UpdateCostCode.Clear();
                txtCostPrice_UpdateCostCode.Clear();
            }
        }

        private void btnDeleteCostCode_Click(object sender, RoutedEventArgs e)
        {
            Cost_RMS cost = new Cost_RMS();
            cost.Cost_Code = Cost_Code_txtbx_delete.Text;

            if (cost.Cost_Code == null || cost.Cost_Code == "")
            {
                MessageBox.Show("A Cost Code must be entered in order for this to function correctly.", "Missing Jumper ID", MessageBoxButton.OK);
            }
            else
            {
                Cost_DB deleteCostDB = new Cost_DB();
                deleteCostDB.Delete_Cost(cost);

                Cost_Code_txtbx_delete.Clear();
            }
        }

        private void btnSearchCostCode_Click(object sender, RoutedEventArgs e)
        {
            Cost_RMS cost = new Cost_RMS();
            cost.Cost_Code = Cost_Code_txtbx_search.Text;

            if (cost.Cost_Code == null || cost.Cost_Code == "")
            {
                MessageBox.Show("A Cost Code must be entered in order for this to function correctly.", "Missing Jumper ID", MessageBoxButton.OK);
            }
            else
            {
                Cost_DB searchCostDB = new Cost_DB();
                DataTable dt = searchCostDB.Retrieve_CostForDisplay(cost);
                dataGridCostCode.ItemsSource = dt.DefaultView;

                Cost_Code_txtbx_search.Clear();
            }
        }

        private void btnSearchAllCosts_Click(object sender, RoutedEventArgs e)
        {
            Cost_DB allCosts = new Cost_DB();
            DataTable dt = allCosts.All_Costs();
            dataGridCostCode.ItemsSource = dt.DefaultView;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

            switch (tabItem)
            {
                case "Update Cost Code":
                    Update_Window uw = new Update_Window();
                    uw.Table_ID_Lbl.Content = "Cost Code:";

                    try
                    {
                        uw.ShowDialog();
                    }
                    catch(Exception excp)
                    {
                        return;
                    }

                    string Cost_Code_Update = "";
                    Cost_Code_Update = uw.ID_Return;
                    uw.Close();

                    if (Cost_Code_Update == null || Cost_Code_Update == "")
                    {
                        return;
                    }
                    else
                    {
                        Cost_RMS cost = new Cost_RMS();
                        cost.Cost_Code = Cost_Code_Update;
                        Cost_DB ret_DB = new Cost_DB();
                        List<string> retrievedCostUpdate = ret_DB.Retrieve_CostForUpdate(cost);
                        try
                        {
                            cost_code_lbl_update.Content = retrievedCostUpdate[0];
                            txtCostName_UpdateCostCode.Text = retrievedCostUpdate[1];
                            txtCostPrice_UpdateCostCode.Text = retrievedCostUpdate[2];
                        }
                        catch(Exception excp)
                        {
                            MessageBox.Show("The requested data could not be retrieved.", "Error", MessageBoxButton.OK);
                        }

                    }

                    return;

                default:
                    return;
            }
        }
    }
}
